//
//  PicatTabBarController.swift
//  Picat
//
//  Created by Per Sonberg on 2016-03-19.
//  Copyright © 2016 Per Sonberg. All rights reserved.
//

import UIKit

class PicatTabBarController: UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        for i : UITabBarItem in self.tabBar.items! {
            i.title = ""
            i.image!.imageWithColor(UIColor.whiteColor())
            i.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0)
            
            i.image = i.selectedImage?.imageWithColor(UIColor.whiteColor()).imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal)
            
            //In case you wish to change the font color as well
            let attributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]
            i.setTitleTextAttributes(attributes, forState: UIControlState.Normal)
        }
        UITabBar.appearance().tintColor = Settings.sharedManager.yellow
        UITabBar.appearance().barTintColor = Settings.sharedManager.blue
            //i.image?.scaleUIImageToSize(CGSize(width: CGFloat(22), height: CGFloat(32)))

        let eventVC = self.viewControllers![0] as! UINavigationController
        if eventVC.topViewController is EventsViewController {
            let vc = eventVC.topViewController as! EventsViewController
            vc.type = PicatService.viewType.event
            vc.navigationItem.title = "EVENTS"
        }
        
        let timelimitVC = self.viewControllers![1] as! UINavigationController
        print(timelimitVC.topViewController)
        if timelimitVC.topViewController is EventsViewController {
            let vc = timelimitVC.topViewController as! EventsViewController
            vc.type = PicatService.viewType.timelimit
            vc.navigationItem.title = "HAPPENINGS"
        }
        let studentVC = self.viewControllers![2] as! UINavigationController
        if studentVC.topViewController is EventsViewController {
            let vc = studentVC.topViewController as! EventsViewController
            vc.type = PicatService.viewType.student
            vc.navigationItem.title = "SCHOOLS"
        }
    }
}

extension UIImage {
    func imageWithColor(color1: UIColor) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(self.size, false, self.scale)
        color1.setFill()
        
        let context = UIGraphicsGetCurrentContext()! as CGContextRef
        CGContextTranslateCTM(context, 0, self.size.height)
        CGContextScaleCTM(context, 1.0, -1.0);
        CGContextSetBlendMode(context, CGBlendMode.Normal)
        
        let rect = CGRectMake(0, 0, self.size.width, self.size.height) as CGRect
        CGContextClipToMask(context, rect, self.CGImage)
        CGContextFillRect(context, rect)
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext() as UIImage
        UIGraphicsEndImageContext()
        
        return newImage
    }
}
