//
//  getImageFromUrl.swift
//  Picat
//
//  Created by Per Sonberg on 2016-01-21.
//  Copyright © 2016 Per Sonberg. All rights reserved.
//

import Foundation
import Alamofire
import UIKit

class getImageFromUrl: NSObject, NSURLSessionDelegate, NSURLSessionDataDelegate {
    
    // singleton manager
    class var sharedManager: getImageFromUrl {
        struct Singleton {
            static let instance = getImageFromUrl()
        }
        return Singleton.instance
    }
    
    var progress : UIProgressView?
    var buffer : NSMutableData = NSMutableData()
    
    func fetch(url: NSURL, progress : UIProgressView?, completionHandler:(succeeded: Bool, image: UIImage?) -> Void) -> Void {
        
        if progress != nil {
            progress!.progress = 0.0
        }
        
        let session = NSURLSession.sharedSession()
        let request = NSMutableURLRequest(URL: url)
        self.progress = progress
        
        let task = session.dataTaskWithRequest(request, completionHandler: { (data, response, error) -> Void in
            if (error == nil) {
                
                let httpResponse : NSHTTPURLResponse = response as! NSHTTPURLResponse
                if (httpResponse.statusCode == 200) {
                    if data != nil {
                        self.buffer.appendData(data!)
                    }
                    
                    let procent =  Float((data?.length)!) / Float(httpResponse.expectedContentLength)
                    if progress != nil {
                        dispatch_async(dispatch_get_main_queue(), { () -> Void in
                            progress!.progress = procent
                        })
                    }
                }
                
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    let image = UIImage(data: data!)
                    completionHandler(succeeded: true, image: image)
                })
            } else {
                completionHandler(succeeded: false, image: nil)
            }
        })
        task.resume()
    }
   
    
}