//
//  NearByEventsGridView.swift
//  Picat
//
//  Created by Per Sonberg on 2016-01-23.
//  Copyright © 2016 Per Sonberg. All rights reserved.
//
/*
import Foundation
import UIKit

class EventsGridView: EventsViewController, UICollectionViewDataSource, UICollectionViewDelegate, UISearchResultsUpdating, UISearchBarDelegate, UISearchControllerDelegate {
    
    var events = [Event]()
    var filteredEvents = [Event]()
    var selectedIndexIdentifier : String?
    var resultSearchController : UISearchController = UISearchController()
    var collectionViewLayout: CustomImageFlowLayout!
    var refreshControl : UIRefreshControl = UIRefreshControl()
    
    // MARK: Outlets
    @IBOutlet weak var CollectionView: UICollectionView!
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        self.prepearData(PicatService.sharedManager.returnEvents())
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.CollectionView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
        
        self.CollectionView.allowsMultipleSelection = false
        collectionViewLayout = CustomImageFlowLayout()
        self.CollectionView.collectionViewLayout = collectionViewLayout
        
        self.resultSearchController = UISearchController(searchResultsController: nil)
        self.resultSearchController.delegate = self
        self.resultSearchController.searchResultsUpdater = self
        self.resultSearchController.dimsBackgroundDuringPresentation = false
        self.resultSearchController.searchBar.sizeToFit()
        self.resultSearchController.hidesNavigationBarDuringPresentation = false;
        self.definesPresentationContext = false
        self.resultSearchController.searchResultsUpdater = self
        super.view.addSubview(self.resultSearchController.searchBar)
        
        self.refreshControl.backgroundColor = UIColor.lightGrayColor()
        self.refreshControl.tintColor = UIColor.whiteColor()
        self.refreshControl.addTarget(self, action: "refreshData:", forControlEvents: UIControlEvents.ValueChanged)
        self.CollectionView.addSubview(refreshControl)
    }
    
    func refreshData(sender: AnyObject) {
        self.prepearData(PicatService.sharedManager.returnEvents())
        self.CollectionView.reloadData()
        self.refreshControl.endRefreshing()
    }
    
    // Prepear data 
    
    func prepearData(data : [String : AnyObject]) {
        
        self.events = [Event]()
        for l in (data["limit"]) as! NSArray {
            self.events += [Event(
                id: (l.valueForKey("id") as! String),
                title: (l.valueForKey("title") as! String),
                desc: (l.valueForKey("desc") as? String),
                thumb: (l.valueForKey("thumb") as! String),
                distance : (l.valueForKey("distance") as? String),
                owner_company : (l.valueForKey("owner_company") as! String),
                event_type : (l.valueForKey("event_type") as? String),
                date_start : (l.valueForKey("date_start") as? String),
                date_stop : (l.valueForKey("date_stop") as? String),
                latitude : (l.valueForKey("latitude") as? NSString)!.doubleValue,
                longitude : (l.valueForKey("longitude") as? NSString)!.doubleValue
                )]
           
        }
        
        self.filteredEvents = [Event]()
        for a in (data["all"]) as! NSArray {
            self.filteredEvents += [Event(
                id: (a.valueForKey("id") as! String),
                title: (a.valueForKey("title") as! String),
                desc: (a.valueForKey("desc") as? String),
                thumb: (a.valueForKey("thumb") as! String),
                distance : (a.valueForKey("distance") as? String),
                owner_company : (a.valueForKey("owner_company") as! String),
                event_type : (a.valueForKey("event_type") as? String),
                date_start : (a.valueForKey("date_start") as? String),
                date_stop : (a.valueForKey("date_stop") as? String),
                latitude : (a.valueForKey("latitude") as? NSString)!.doubleValue,
                longitude : (a.valueForKey("longitude") as? NSString)!.doubleValue
                )]
        }
        
        self.CollectionView.reloadData()
    }
    
    // MARK: Grid view
    
    /*
    
    Number of sections
    
    */
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    /*
    
    Number of posts
    
    */
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.resultSearchController.active && self.resultSearchController.searchBar.text?.characters.count != 0 {
            return filteredEvents.count
        } else {
            return events.count
        }
    }
    
    /*
    
    Give value to fields
    
    */
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cellGrid = collectionView.dequeueReusableCellWithReuseIdentifier("grid", forIndexPath: indexPath) as UICollectionViewCell
        
        var event : Event?
        if self.resultSearchController.active && self.resultSearchController.searchBar.text?.characters.count != 0 {
            event = filteredEvents[indexPath.row]
        } else {
            event = events[indexPath.row]
        }

        let imageView = cellGrid.viewWithTag(1) as! UIImageView
        
        getImageFromUrl.sharedManager.fetch(NSURL(string: event!.thumb)!, completionHandler: { succeeded, image in
            if succeeded == true {
                imageView.image = image
            } else {
                imageView.image = UIImage(named: "harrys_011")
            }
            
        })
        return cellGrid
    }
    
    /*
    
    Post is clicked
    
    */
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        var event : Event?
        if self.resultSearchController.active && self.resultSearchController.searchBar.text?.characters.count != 0 {
            event = filteredEvents[indexPath.row]
        } else {
            event = events[indexPath.row]
        }
        
        self.selectedIndexIdentifier = event!.id
        performSegueWithIdentifier("ShowPostsFromEventSegue", sender: self)
        self.CollectionView.deselectItemAtIndexPath(indexPath, animated: false)
    }
    
    // MARK: Search events
    
    /*
    
    Show cancel button
    
    */
    func didPresentSearchController(searchController: UISearchController) {
        searchController.searchBar.showsCancelButton = true
    }
    
    /*
    
    Filter matching posts
    
    */
    func filterContentForSearchText(searchText: String, scope: String = "All") {
        self.filteredEvents = self.events.filter { event in
            return event.title.lowercaseString.containsString(searchText.lowercaseString)
        }
        CollectionView.reloadData()
    }
    
    /*
    
    Search text is updated
    
    */
    func updateSearchResultsForSearchController(searchController: UISearchController) {
        filterContentForSearchText(self.resultSearchController.searchBar.text!, scope: "All")
    }
    
    // MARK: Send data with segue
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "ShowPostsFromEventSegue" {
            let DestViewController : PostViewController = segue.destinationViewController as! PostViewController
            DestViewController.RecivedIdentifier = self.selectedIndexIdentifier
            
        }
    }

    
}
*/
