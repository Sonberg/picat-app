//
//  Post.swift
//  Picat
//
//  Created by Per Sonberg on 2016-01-10.
//  Copyright © 2016 Per Sonberg. All rights reserved.
//

import Foundation
import UIKit

struct Post {
    //let user : String
    var id : Int?
    let owner_event : Int?
    let text : String?
    var url : String?
    var likes : Int? = 0
    var liked : Int = 0
    var created_at : String = ""
}
