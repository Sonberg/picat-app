//
//  SettingsViewController.swift
//  Picat
//
//  Created by Per Sonberg on 2016-03-27.
//  Copyright © 2016 Per Sonberg. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit

class SettingsViewController: UIViewController, FBSDKLoginButtonDelegate, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    let loginButton = FBSDKLoginButton()
    
    // Hide Status bar
    override func prefersStatusBarHidden() -> Bool {
        return true;
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        
        loginButton.delegate = self
        
        //self.view.addSubview(loginButton)
    }

    @IBAction func closeButtonPressed(sender: AnyObject) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func readPolicyButtonPressed(sender: AnyObject) {
        UIApplication.sharedApplication().openURL(NSURL(string: "https://sheltered-lowlands-88850.herokuapp.com/policy")!)
    }

    
    func loginButton(loginButton: FBSDKLoginButton!, didCompleteWithResult result: FBSDKLoginManagerLoginResult!, error: NSError!) {
        
    }
    
    func calcFBButtonPos() -> CGPoint {
        let screen = UIScreen.mainScreen().bounds
        let center = self.view.center
        let pos : CGPoint = CGPoint(x: center.x, y: screen.height - 100)
        return pos
    }
    
    func loginButtonDidLogOut(loginButton: FBSDKLoginButton!) {
        self.performSegueWithIdentifier("UserDidLogout", sender: nil)
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCellWithIdentifier("cellSettings", forIndexPath: indexPath) as UITableViewCell
        
        switch indexPath.section {
        case 0:
            cell.textLabel?.text = "Terms of use (EULA)"
            break
        case 1:
            
             self.loginButton.center = CGPointMake(tableView.bounds.width/2, self.loginButton.bounds.height / 1.4)
             cell.addSubview(self.loginButton)
             cell.accessoryType = UITableViewCellAccessoryType.None
             cell.selectionStyle = UITableViewCellSelectionStyle.None
             cell.textLabel?.text = ""
            break
        default:
            break
        }
        
        return cell
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            return "Information"
        }
        return ""
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.section == 0 {
            performSegueWithIdentifier("webViewSeque", sender: nil)
        }
        
        self.tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
    
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "webViewSeque" {
            let VC = segue.destinationViewController as! WebViewController
            VC.pageTitle = "Terms of use (EULA)"
            VC.url = "https://sheltered-lowlands-88850.herokuapp.com/policy"
        }
    }
    
}
