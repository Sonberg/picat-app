//
//  PostViewController.swift
//  Picat
//
//  Created by Per Sonberg on 2016-01-10.
//  Copyright © 2016 Per Sonberg. All rights reserved.
//

import UIKit
import Alamofire
import FBSDKCoreKit

class PostViewController: UIViewController {
    
    var RecivedIdentifier : String?
    var EventType : String?
    
    var posts = [Post]()
    var tableVC : PostListedInTableViewController?
    var gridVC : PostListedInGridView?
    var lastPost : Int = 0
    
    enum fetchType {
        case reload
        case append
    }
    
    
    // MARK: Outlets
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var toggleViewButton: UIBarButtonItem!
    
    // MARK: Actions
    @IBAction func DismissPostViewControllerButtonPressed(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func changeViewButtonPressed(sender: AnyObject) {
        if self.containerView.hidden {
            self.containerView.hidden = false
            self.toggleViewButton.image = UIImage(named: "grid")
        } else {
            self.containerView.hidden = true
            self.toggleViewButton.image = UIImage(named: "list")
        }
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        if self.RecivedIdentifier == nil {
            self.dismissViewControllerAnimated(true, completion: nil)
        } else {
            NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(PostViewController.refreshList(_:)),name:"refresh", object: nil)
            NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(PostViewController.ScrollTriggerd(_:)),name:"scroll", object: nil)
            
            // Fetch and send obejcts
            self.fetchPosts(.reload)
        }
        
    }
    
    func ScrollTriggerd(notification: NSNotification){
        if let last = notification.userInfo!["last"] {
            self.lastPost = Int(last as! NSNumber)
            self.fetchPosts(.append)
        }
    }
    
    func refreshList(notification: NSNotification){
        self.lastPost = 0
        self.fetchPosts(.reload)
    }
    
    
    func fetchPosts(type : fetchType) {
        let req = FBSDKGraphRequest(graphPath: "me", parameters: ["fields":"email,name,picture"])
        req.startWithCompletionHandler({ (connection, result, error : NSError!) -> Void in
            if(error == nil) {
                
                let id : String = result.valueForKey("id") as! String
                let url = Settings.sharedManager.url + "event/" + self.EventType! + "/" + self.RecivedIdentifier! + "/" +  id + "/" + String(self.lastPost)
                print(url)
                Alamofire.request(.GET, url, headers: Settings.sharedManager.headers)
                    .responseJSON { response in
                        if let JSON = response.result.value {
                            let data = ["json" : JSON ]
                            switch type {
                            case .reload:
                                NSNotificationCenter.defaultCenter().postNotificationName("reload", object: nil, userInfo: data)
                                break
                                
                            case .append:
                                NSNotificationCenter.defaultCenter().postNotificationName("append", object: nil, userInfo: data)
                                break
                            }
                
                        } else {
                            print(error)
                        }
                }
            }
        })
    }
    
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == "EmbeddedTableView" {
            self.tableVC = segue.destinationViewController as? PostListedInTableViewController
            self.tableVC!.EventType = self.EventType!

        }
        
        if segue.identifier == "EmbeddedPostGrid" {
            self.gridVC = segue.destinationViewController as? PostListedInGridView
            self.gridVC!.EventType = self.EventType!
        }
        
    }
    
    func adaptivePresentationStyleForPresentationController(controller: UIPresentationController) -> UIModalPresentationStyle {
        return .None
    }
    
}
