//
//  SingelTableViewController.swift
//  Picat
//
//  Created by Per Sonberg on 2016-03-21.
//  Copyright © 2016 Per Sonberg. All rights reserved.
//

import UIKit
import SwiftDate
import Alamofire
import Foundation
import Kingfisher
import SimpleAlert
import FBSDKCoreKit

class SingelTableViewController: UIViewController {
    
    // MARK: Outlets
    @IBOutlet weak var tableView: UITableView!
    
    var postType : String = ""
    var recivedPost : Post? = nil
    let dateFormatter = NSDateFormatter()
    var refreshControl : UIRefreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.rowHeight = CGFloat(UIScreen.mainScreen().bounds.width + 64.0)
        self.refreshControl.backgroundColor = UIColor.whiteColor()
        self.refreshControl.tintColor = Settings.sharedManager.darkYellow
        self.refreshControl.addTarget(self, action: #selector(SingelTableViewController.refreshData(_:)), forControlEvents: UIControlEvents.ValueChanged)
        self.tableView.alwaysBounceVertical = true
        self.automaticallyAdjustsScrollViewInsets = false
        self.tableView.addSubview(refreshControl)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        let cache = KingfisherManager.sharedManager.cache
        cache.cleanExpiredDiskCache()
    }

    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    // Refresh information
    func refreshData(sender: AnyObject) {
        self.tableView.reloadData()
        self.refreshControl.endRefreshing()
    }
    
    //Option Button Pressed
    func optionButtonPressed(sender: UIButton!) {
        let action = SimpleAlert.Controller(title: nil, message: nil, style: .ActionSheet)
        action.addAction(SimpleAlert.Action(title: "Report", style: .Destructive) { action in
            self.reportAlert()
            })
        
        action.addAction(SimpleAlert.Action(title: "Cancel", style: .Cancel))
        presentViewController(action, animated: true, completion: nil)
    }
    
    
    // Like Button Pressed
    func likeButtonPressed(sender:UIButton!) {
        sender.setBackgroundImage(UIImage(named: "loading"), forState: UIControlState.Normal)
        sender.rotate360Degrees()
        
        // Fetching user id
        let req = FBSDKGraphRequest(graphPath: "me", parameters: ["fields":"email,name,picture"])
        req.startWithCompletionHandler({ (connection, result, error : NSError!) -> Void in
            if(error == nil) {
                let user : String = result.valueForKey("id") as! String
                if let id : String = String(self.recivedPost!.id! as NSNumber) {

                    Alamofire.request(.GET, Settings.sharedManager.url + "post/" + id + "/like/" + user + "/" + self.postType, headers: ["X-Token" : Settings.sharedManager.token])
                        .responseJSON { response in
                            if let JSON = response.result.value {
                                // Action after response
                                if self.recivedPost != nil {
                                    if JSON.valueForKey("liked") as! Int == 1 {
                                        self.recivedPost?.liked = 1
                                        self.recivedPost?.likes = self.recivedPost!.likes!+1
                                    }
                                    else {
                                        self.recivedPost?.liked = 0
                                        self.recivedPost?.likes = self.recivedPost!.likes!-1
                                    }
                                    self.tableView.reloadData()
                                }
                            }
                    }
                } else {
                    print(error)
                }
            }
        })
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! PostCell
        let post = self.recivedPost
        
        let img = cell.viewWithTag(10) as! UIImageView
        
        // Amount of likes
        cell.likesLabel.text = (post!.likes! as NSNumber).stringValue
        
        // Created at
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date = dateFormatter.dateFromString(post!.created_at)?.add(0, months: 0, weeks: 0, days: 0, hours: 2, minutes: 0, seconds: 0, nanoseconds: 0)
        
        if post!.created_at != "" && date != nil {
            cell.TopBarLabel.text = timeAgoSince(date!)
        } else {
            cell.TopBarLabel.text = ""
        }
        
        cell.optionButton.tag = indexPath.row
        cell.optionButton.addTarget(self, action: #selector(PostListedInTableViewController.optionButtonPressed(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        
        cell.likeButton.tag = indexPath.row
        cell.likeButton.addTarget(self, action: #selector(SingelTableViewController.likeButtonPressed(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        
        // Setting like state
        if post!.liked != 0 {
            cell.likeButton.setBackgroundImage(UIImage(named: "Star-Fill"), forState: UIControlState.Normal)
        } else {
            cell.likeButton.setBackgroundImage(UIImage(named: "Star-Empty"), forState: UIControlState.Normal)
        }
        
        
        // Setup
        let progressView = cell.viewWithTag(9) as! UIProgressView
        progressView.progress = Float(0)
        progressView.alpha = 1
        
        img.kf_setImageWithURL(NSURL(string: post!.url!)!,
                               placeholderImage: UIImage(named: "placeholder"),
                               optionsInfo: nil,
                               progressBlock: { (receivedSize, totalSize) -> () in
                                progressView.progress = Float(receivedSize) / Float(totalSize)
                                print(progressView.progress)
            },
                               completionHandler: { (image, error, cacheType, imageURL) -> () in
                                img.image = image
                                progressView.alpha = 0
        })
        return cell
        
    }
    
    func reportAlert() {
        let alert = SimpleAlert.Controller(title: "Report Image", message: nil, style: .Alert)
        
        alert.addTextFieldWithConfigurationHandler()
        alert.addAction(SimpleAlert.Action(title: "Cancel", style: .Cancel))
        alert.addAction(SimpleAlert.Action(title: "OK", style: .Destructive) { action in
            let text = String(UTF8String: alert.textFields[0].text!)!
            self.flagContent(text)
            })
        
        presentViewController(alert, animated: true, completion: nil)
    }
    
    // MARK: - Flag Content
    func flagContent(text: String) -> Void {
        // Fetching user id
        let req = FBSDKGraphRequest(graphPath: "me", parameters: ["fields":"email,name,picture"])
        req.startWithCompletionHandler({ (connection, result, error : NSError!) -> Void in
            if(error == nil) {
                let user : String = result.valueForKey("id") as! String
                
                // Callling server
                Alamofire.request(.POST, Settings.sharedManager.url + "post/" + String(self.recivedPost!.id! as NSNumber) + "/flag/" + user + "/" + self.postType, headers: ["X-Token" : Settings.sharedManager.token], parameters: ["text" : text])
                    .responseJSON { response in
                        if let JSON = response.result.value {
                            print(JSON)
                        }
                }
            } else {
                print(error)
            }
        })
    }

}
