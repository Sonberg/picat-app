//
//  PostListedInTableViewController.swift
//  Picat
//
//  Created by Per Sonberg on 2016-01-22.
//  Copyright © 2016 Per Sonberg. All rights reserved.
//

import UIKit
import SwiftDate
import Alamofire
import Foundation
import Kingfisher
import SimpleAlert
import FBSDKCoreKit
import UIScrollView_InfiniteScroll

class PostListedInTableViewController: PostViewController, UIGestureRecognizerDelegate {
    
    // MARK: Outlets
    @IBOutlet weak var tableView: UITableView!
    
    var recivedPosts = [Post]()
    var actionIndex : String = ""
    var refreshControl : UIRefreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(PostListedInTableViewController.listRecived(_:)),name:"reload", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(PostListedInTableViewController.listRecived(_:)),name:"append", object: nil)
        
        self.tableView.rowHeight = CGFloat(UIScreen.mainScreen().bounds.width + 64.0)
        self.refreshControl.backgroundColor = UIColor.whiteColor()
        self.refreshControl.tintColor = Settings.sharedManager.darkYellow
        self.refreshControl.addTarget(self, action: #selector(PostListedInTableViewController.refreshData(_:)), forControlEvents: UIControlEvents.ValueChanged)
        self.tableView.alwaysBounceVertical = true;
        self.tableView.addSubview(refreshControl)
        
        self.tableView.infiniteScrollIndicatorStyle = .White
        self.tableView.infiniteScrollIndicatorView = InfiniteIndicator(frame: CGRectMake(0, 0, 44, 44))
        
        self.tableView.addInfiniteScrollWithHandler { (scrollView) -> Void in
            let tableView = scrollView as! UITableView
            
            let data = ["last" : self.recivedPosts.last?.id as! AnyObject ]
            NSNotificationCenter.defaultCenter().postNotificationName("scroll", object: nil, userInfo: data)
            
            // finish infinite scroll animation
            tableView.finishInfiniteScroll()
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        let cache = KingfisherManager.sharedManager.cache
        cache.cleanExpiredDiskCache()
    }

    
    // Refresh information
    func refreshData(sender: AnyObject) {
        NSNotificationCenter.defaultCenter().postNotificationName("refresh", object: nil)
        self.tableView.reloadData()
        self.refreshControl.endRefreshing()
    }
    
    // Recive information
    func listRecived(notification: NSNotification){
        switch notification.name {
            case "append":
                break
            
            default:
                self.recivedPosts = [Post]()
                break
        }
        
        if (notification.userInfo!["json"] as! NSArray).count > 0 {
            for p in notification.userInfo!["json"] as! NSArray {
                self.recivedPosts += [Post(
                    id: (p.valueForKey("id") as? Int),
                    owner_event: (p.valueForKey("owner_event") as? Int),
                    text: (p.valueForKey("text") as? String),
                    url: (p.valueForKey("url") as? String),
                    likes: (p.valueForKey("likes") as? Int),
                    liked: (p.valueForKey("liked") as? Int)!,
                    created_at: (p.valueForKey("created_at") as? String)!
                    )]
            }
            self.tableView.reloadData()
        }
        
        if self.tableView.numberOfRowsInSection(0) != self.recivedPosts.count {
            self.tableView.reloadData()
        }
    }
    //Option Button Pressed
    func optionButtonPressed(sender: UIButton!) {
        self.actionIndex = String(self.recivedPosts[sender.tag].id! as NSNumber)
        
        let action = SimpleAlert.Controller(title: nil, message: nil, style: .ActionSheet)
        action.addAction(SimpleAlert.Action(title: "Report", style: .Destructive) { action in
            self.reportAlert()
        })
        
        action.addAction(SimpleAlert.Action(title: "Cancel", style: .Cancel))
        presentViewController(action, animated: true, completion: nil)
    }
    
    // Like Button Pressed
    func likeButtonPressed(sender : AnyObject) {
        print("like")
        if sender is UIButton {
            (sender as! UIButton).setBackgroundImage(UIImage(named: "loading"), forState: UIControlState.Normal)
            (sender as! UIButton).rotate360Degrees()
        }
        
        // Fetching user id
        let req = FBSDKGraphRequest(graphPath: "me", parameters: ["fields":"email,name,picture"])
        req.startWithCompletionHandler({ (connection, result, error : NSError!) -> Void in
            if(error == nil) {

                let user : String = result.valueForKey("id") as! String
                if let id : String = String(self.recivedPosts[sender.tag].id! as NSNumber) {
                    // Callling server
                    Alamofire.request(.GET, Settings.sharedManager.url + "post/" + id + "/like/" + user + "/" + self.EventType!, headers: ["X-Token" : Settings.sharedManager.token])
                        .responseJSON { response in
                            if let JSON = response.result.value {
                                
                                // Action after response
                                if JSON.valueForKey("liked") as! Int == 1 {
                                    self.recivedPosts[sender.tag].liked = 1
                                    self.recivedPosts[sender.tag].likes = self.recivedPosts[sender.tag].likes!+1
                                }
                                else {
                                    self.recivedPosts[sender.tag].liked = 0
                                    self.recivedPosts[sender.tag].likes = self.recivedPosts[sender.tag].likes!-1
                                }
                                self.tableView.reloadData()
                            }
                    }
                } else {
                    print(error)
                }
            }
        })
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.recivedPosts.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! PostCell
        let post = self.recivedPosts[indexPath.row]
        
        let img = cell.viewWithTag(10) as! UIImageView
        
        // Amount of likes
        cell.likesLabel.text = (post.likes! as NSNumber).stringValue
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date = dateFormatter.dateFromString(post.created_at)?.add(0, months: 0, weeks: 0, days: 0, hours: 2, minutes: 0, seconds: 0, nanoseconds: 0)
        
        if post.created_at != "" && date != nil {
            cell.TopBarLabel.text = timeAgoSince(date!)
        } else {
                cell.TopBarLabel.text = ""
        }
 
        
        cell.optionButton.tag = indexPath.row
        cell.optionButton.addTarget(self, action: #selector(PostListedInTableViewController.optionButtonPressed(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        
        cell.likeButton.tag = indexPath.row
        cell.likeButton.addTarget(self, action: #selector(PostListedInTableViewController.likeButtonPressed(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        
        // Setting like state
        if post.liked == 1 {
            cell.likeButton.setBackgroundImage(UIImage(named: "Star-Fill"), forState: UIControlState.Normal)
        } else {
            cell.likeButton.setBackgroundImage(UIImage(named: "Star-Empty"), forState: UIControlState.Normal)
        }

        // Setup
        let progressView : UIProgressView? = cell.viewWithTag(9) as? UIProgressView
        progressView?.progress = Float(0)
        progressView?.alpha = 1
        
        img.kf_setImageWithURL(NSURL(string: post.url!)!,
                                     placeholderImage: UIImage(named: "event-placeholder"),
                                     optionsInfo: nil,
                                     progressBlock: { (receivedSize, totalSize) -> () in
                                        progressView?.progress = Float(receivedSize) / Float(totalSize)
            },
                                     completionHandler: { (image, error, cacheType, imageURL) -> () in
                                        img.image = image
                                        progressView?.alpha = 0
            })
        return cell
        
    }
    
    func reportAlert() {
        let alert = SimpleAlert.Controller(title: "Report Image", message: nil, style: .Alert)
        
        alert.addTextFieldWithConfigurationHandler()
        alert.addAction(SimpleAlert.Action(title: "Cancel", style: .Cancel))
        alert.addAction(SimpleAlert.Action(title: "OK", style: .Destructive) { action in
                let text = String(UTF8String: alert.textFields[0].text!)!
                self.flagContent(text)
            })
        
        presentViewController(alert, animated: true, completion: nil)
    }
    
    // MARK: - Flag Content
    func flagContent(text: String) -> Void {
        // Fetching user id
        let req = FBSDKGraphRequest(graphPath: "me", parameters: ["fields":"email,name,picture"])
        req.startWithCompletionHandler({ (connection, result, error : NSError!) -> Void in
            if(error == nil) {
                let user : String = result.valueForKey("id") as! String
                
                // Callling server
                Alamofire.request(.POST, Settings.sharedManager.url + "post/" + self.actionIndex + "/flag/" + user + "/" + self.EventType!, headers: ["X-Token" : Settings.sharedManager.token], parameters: ["text" : text])
                .responseJSON { response in
                        if let JSON = response.result.value {
                            print(JSON)
                    }
                }
                } else {
                    print(error)
                }
        })
    }

}
