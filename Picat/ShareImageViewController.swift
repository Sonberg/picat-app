//
//  ShareImageViewController.swift
//  Picat
//
//  Created by Per Sonberg on 2016-01-12.
//  Copyright © 2016 Per Sonberg. All rights reserved.
//

import UIKit
import Social
import Accounts
import Alamofire

import FBSDKCoreKit
import FBSDKShareKit

class ShareImageViewController: UIViewController, UITextFieldDelegate {
    
    var TWActive : Bool = false
    var FBActive : Bool = false
    var IGActive : Bool = false
    var RecivedImage : UIImage?
    var eventType : String?
    var ownerEvent : String?
    
    // MARK: Outlets
    @IBOutlet weak var ImageView: UIImageView!
    @IBOutlet weak var twitterButton: UIButton!
    @IBOutlet weak var facebookButton: UIButton!
    @IBOutlet weak var instagramButton: UIButton!
    @IBOutlet weak var textInput: UITextField!
    @IBOutlet weak var progressBar: UIProgressView!
    @IBOutlet weak var shareButton: UIButton!
    
    // Dissmiss View
    @IBAction func CloseShareViewButtonPressed(sender: AnyObject) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    // MARK : Toggle share button state
    @IBAction func toggleTwitter(sender: AnyObject) {
        TWActive = !TWActive
        if TWActive == true {
            twitterButton.setBackgroundImage(UIImage(named: "twitter-1"), forState: UIControlState.Normal)
        } else {
            twitterButton.setBackgroundImage(UIImage(named: "twitter-inactive"), forState: UIControlState.Normal)
        }
        self.checkServicesAvalible()
    }
    @IBAction func toggleFacebook(sender: AnyObject) {
        FBActive = !FBActive
        if FBActive == true {
            facebookButton.setBackgroundImage(UIImage(named: "facebook-1"), forState: UIControlState.Normal)
        } else {
            facebookButton.setBackgroundImage(UIImage(named: "facebook-inactive"), forState: UIControlState.Normal)
        }
        self.checkServicesAvalible()
    }
    @IBAction func toggleInstagram(sender: AnyObject) {
        IGActive = !IGActive
        if IGActive == true {
            instagramButton.setBackgroundImage(UIImage(named: "instagram"), forState: UIControlState.Normal)
        } else {
            instagramButton.setBackgroundImage(UIImage(named: "instagram-inactive"), forState: UIControlState.Normal)
        }
        self.checkServicesAvalible()
    }
    
    // Submit picture and share
    @IBAction func ShareButtonPressed(sender: AnyObject) {
        let image : UIImage = ImageUtil.cropToSquare(image: self.ImageView.image!)
        let text : String = self.textInput.text!
        
        self.shareToPicat(image, text: text, progress: self.progressBar)

    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    // Hide Status bar
    override func prefersStatusBarHidden() -> Bool {
        return true;
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        checkServicesAvalible()
        
        ImageView.image = self.RecivedImage
        self.progressBar.progress = 0
        self.textInput.delegate = self
        
        // Append Overlay
        let overLayViews = OverlayCropToSquare.sharedManager.createOverlay()
        view.addSubview(overLayViews.0)
        view.insertSubview(overLayViews.0, atIndex: 1)
        
        view.addSubview(overLayViews.1)
        view.insertSubview(overLayViews.1, atIndex: 1)
        
    }
    
    func shareToTwitter(image : UIImage, text : String) {
        if TWActive {
        TwitterManager.sharedManager.postToTwitter(image, caption: text)
        }
    }
    func shareToFacebook(image : UIImage, text : String) {
        if FBActive {
            FacebookManager.sharedManager.postToFacebook(image, caption: text, view: self.view)
        }
    }
    func shareToInstagram(image : UIImage, text : String) {
        if IGActive {
            InstagramManager.sharedManager.postImageToInstagramWithCaption(image, instagramCaption: text, view: self.view)
        }
    }
    
    func shareToPicat(image : UIImage, text : String, progress : UIProgressView) {
        self.shareButton.enabled = false
        self.shareToTwitter(image, text: text)
        PicatService.sharedManager.PostImage(image, text: text, type: self.eventType!, owner: self.ownerEvent!, progress: progress) { (succeeded) -> Void in
            if succeeded == true {
                dispatch_async(dispatch_get_main_queue()) {
                    self.shareToFacebook(image, text: text)
                    if self.IGActive {
                        self.shareToInstagram(image, text: text)
                    } else {
                        self.presentingViewController!.presentingViewController!.dismissViewControllerAnimated(true, completion: nil)
                    }
                }
            } else {
                self.shareButton.enabled = true
            }
        }
    }
    
    
    func checkServicesAvalible() {
        
        // Twitter Service
        if TwitterManager.sharedManager.ServiceAvalible() {
            self.twitterButton.enabled = true
        } else {
            self.twitterButton.enabled = false
        }
        
        // Facebook Service
        if FacebookManager.sharedManager.ServiceAvalible() {
            self.facebookButton.enabled = true
        } else {
            self.facebookButton.enabled = false
        }
        
        // Instagram Service
        if InstagramManager.sharedManager.ServiceAvalible() {
            self.instagramButton.enabled = true
        } else {
            self.instagramButton.enabled = false
        }
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    

}




//ImageUtil.cropToSquare(image: self.ImageView.image!) // Crop image befor share