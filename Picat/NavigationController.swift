//
//  NavigationController.swift
//  Picat
//
//  Created by Per Sonberg on 2016-03-21.
//  Copyright © 2016 Per Sonberg. All rights reserved.
//

import UIKit
import Foundation

class NavigationController: UINavigationController {
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationBar.hideBottomHairline()
        
        // Set global styles / settings for navigation bar
        UINavigationBar.appearance().tintColor = Settings.sharedManager.blue
        UINavigationBar.appearance().barTintColor = UIColor.whiteColor()
        
        
        // Set title color
        self.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: Settings.sharedManager.blue]
        
        for i : UINavigationItem in self.navigationBar.items! {
            i.rightBarButtonItem?.image!.resize(CGSize(width: 44, height: 44), completionHandler: { (resizedImage, data) -> () in
                //i.title = ""
                i.rightBarButtonItem?.image! = resizedImage
                
            })
        }
        self.popViewControllerAnimated(false)
    }
}

extension UINavigationBar {
    
    func hideBottomHairline() {
        let imageView: UIImageView? = findHairlineImageViewunder(self)
        if (imageView != nil) {
            imageView!.hidden = true
        }
    }
    
    func showBottomHairline() {
        let imageView: UIImageView? = findHairlineImageViewunder(self)
        if (imageView != nil) {
            imageView!.hidden = false
        }
    }
    
    func findHairlineImageViewunder(view: UIView) -> UIImageView? {
        if (view is UIImageView) && (1.0 >= view.bounds.size.height) {
            return view as? UIImageView
        }
        for subView: AnyObject in view.subviews {
            let imageView: UIImageView? = findHairlineImageViewunder(subView as! UIView)
            if (imageView != nil) {
                return imageView
            }
        }
        return nil
    }
}