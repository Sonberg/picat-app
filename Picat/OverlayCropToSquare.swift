//
//  OverlayCropToSquare.swift
//  Picat
//
//  Created by Per Sonberg on 2016-01-15.
//  Copyright © 2016 Per Sonberg. All rights reserved.
//

import UIKit

class OverlayCropToSquare {
    
    class var sharedManager: OverlayCropToSquare {
        struct Singleton {
            static let instance = OverlayCropToSquare()
        }
        return Singleton.instance
    }
    
    init() {
        let screenRect : CGRect  = UIScreen.mainScreen().bounds;
        let ScreenSize = screenRect.size
        self.SquareHeigth = (CGFloat(ScreenSize.height) - CGFloat(ScreenSize.width) + CGFloat(0)) / CGFloat(2)
    }
    
    let color = UIColor(red: (89/255.0), green: (166/255.0), blue: (228/255.0), alpha: 1.0)
    var SquareHeigth : CGFloat?
    
    func createOverlay(alpha : CGFloat = CGFloat(1)) -> (UIView, UIView) {
        return (self.topOverlay(self.SquareHeigth!, alpha: alpha), self.bottomOverlay(self.SquareHeigth!, alpha: alpha))
    }
    
    func topOverlay(newHeight : CGFloat, alpha : CGFloat = CGFloat(1)) -> UIView {
        let OverlayViewTop = UIView(frame: CGRectMake(0, 0, UIScreen.mainScreen().bounds.width, newHeight))
        OverlayViewTop.backgroundColor = self.color
        OverlayViewTop.alpha = alpha
        return OverlayViewTop
        
    }
    
    func bottomOverlay(newHeight : CGFloat, alpha : CGFloat = CGFloat(1)) -> UIView {
        let OverlayViewBottom = UIView(frame: CGRectMake(0, (UIScreen.mainScreen().bounds.height - newHeight), UIScreen.mainScreen().bounds.width, newHeight))
        OverlayViewBottom.backgroundColor = self.color
        OverlayViewBottom.alpha = alpha
        return OverlayViewBottom
    }
    
    func returnHeight() -> CGFloat {
        return self.SquareHeigth!
    }
}
