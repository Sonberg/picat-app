//
//  FacebookManager.swift
//  Picat
//
//  Created by Per Sonberg on 2016-01-20.
//  Copyright © 2016 Per Sonberg. All rights reserved.
//

import Foundation
import UIKit

import FBSDKCoreKit
import FBSDKShareKit


class FacebookManager: NSObject{
    
    class var sharedManager: FacebookManager {
        struct Singleton {
            static let instance = FacebookManager()
        }
        return Singleton.instance
    }
    
    func ServiceAvalible () -> Bool {
        if FBSDKAccessToken.currentAccessToken() != nil {
           return true
        } else {
            return false
        }
    }
    
    func postToFacebook(image: UIImage, caption: String, view: UIView) {
        if ServiceAvalible() {
            let photo : FBSDKSharePhoto = FBSDKSharePhoto()
            photo.image = image
            photo.caption = caption
            photo.userGenerated = true
            let content : FBSDKSharePhotoContent = FBSDKSharePhotoContent()
            content.photos = [photo]
            FBSDKShareAPI.shareWithContent(content, delegate: nil)
        }
    }
}
