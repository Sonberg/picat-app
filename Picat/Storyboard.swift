

class Storyboard {
    
    class func viewControllerFromStoryboard(name: String, withViewControllerIdentifier identifier: String) -> UIViewController {
        var storyBoard = UIStoryboard(name: name, bundle: nil)
        return storyBoard.instantiateViewControllerWithIdentifier(identifier) as UIViewController
    }
    
    class func viewFromNib(name: String) -> UIView! {
        return UINib(nibName: name, bundle: NSBundle.mainBundle()).instantiateWithOwner(nil, options: nil)[0] as? UIView
    }
  
    
}

class Model {
    
    func setRootViewControllerTo( newRootVC:UIViewController ) {
        //dispatch async allows this to be called from inside a completion block
        dispatch_async(dispatch_get_main_queue(), {
            UIApplication.sharedApplication().keyWindow.rootViewController = newRootVC
        })
    }
    
}