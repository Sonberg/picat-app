//
//  Settings.swift
//  Picat
//
//  Created by Per Sonberg on 2016-03-07.
//  Copyright © 2016 Per Sonberg. All rights reserved.
//

import UIKit
import Foundation

class Settings : NSObject {
    
    // singleton manager
    class var sharedManager: Settings {
        struct Singleton {
            static let instance = Settings()
        }
        return Singleton.instance
    }
    
    
    var token : String = "ya0lhlyzmbkpvk1bgckehfd1ym1tb7aii5wd8gp7"
    var url : String = "http://sheltered-lowlands-88850.herokuapp.com/api/"
    var issueMail : String = "issue@picat.se"
    
    var black : UIColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.5)
    var blue : UIColor = UIColor(red: (89/255.0), green: (166/255.0), blue: (228/255.0), alpha: 1.0)
    var grey : UIColor = UIColor(red: 220/255, green: 220/255, blue: 220/255, alpha: 1)
    var lightgrey : UIColor = UIColor(red: 250/255, green: 250/255, blue: 250/255, alpha: 1)
    var yellow : UIColor = UIColor(red: 245/255, green: 226/255, blue: 12/255, alpha: 1)
    var darkYellow : UIColor = UIColor(red: 231/255, green: 191/255, blue: 101/255, alpha: 1)
    
    
    let headers = [
        "X-Token" : "ya0lhlyzmbkpvk1bgckehfd1ym1tb7aii5wd8gp7"
    ]
    
    let EventsAttr = [
        "id" : "id",
        "owner_company" : "owner_company",
        "thumb" : "thumb",
        "title" : "title",
        "distance" : "distance",
        "event_type" : "event_type",
        "date_start" : "date_start",
        "date_stop" : "date_stop",
        "latitude" : "latitude",
        "longitude" : "longitude",
        "desc" : "desc"
    ]
    
    let SchoolAttr = [
        "id" : "id",
        "owner_company" : "owner_company",
        "thumb" : "thumb",
        "title" : "title",
        "latitude" : "latitude",
        "longitude" : "longitude",
        "desc" : "description"
    ]
    
    let TimelimitAttr = [
        "id" : "id",
        "url" : "url",
        "title" : "title",
        "date_start" : "date_start",
        "date_stop" : "date_stop",
        ]
    
    let valueEvent = [
        "owner_event" : "owner_event",
        "url" : "url"
    ]
    
    let valueCountry = [
        "id" : "id",
        "country" : "country",
        "short_name" : "short_name",
        "url" : "url"
    ]
    
    let valueTimelimit = [
        "id" : "id",
        "title" : "title",
        "url" : "url"
    ]
    

}
