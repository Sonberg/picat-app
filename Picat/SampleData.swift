//
//  SampleData.swift
//  Picat
//
//  Created by Per Sonberg on 2016-01-25.
//  Copyright © 2016 Per Sonberg. All rights reserved.
//

import Foundation
import UIKit

class SampleData: NSObject {
    
    // singleton manager
    class var sharedManager: SampleData {
        struct Singleton {
            static let instance = SampleData()
        }
        return Singleton.instance
    }

    var events = [Event]()
    var posts = [Post]()
    /*
    func returnEvents() -> [Event] {
        self.events = [Event]()
        self.events += [Event(id: "harrys_kalmar", title: "Harrys Kalmar", desc: "", url: "https://scontent.cdninstagram.com/hphotos-xfa1/t51.2885-19/10860173_1534817216776886_713569064_a.jpg", thumb: UIImage())]
        self.events += [Event(id: "aik", title: "AIK", desc: "", url: "https://upload.wikimedia.org/wikipedia/en/thumb/2/20/Allm%C3%A4nna_Idrottsklubben_Ishockey_Logo.svg/821px-Allm%C3%A4nna_Idrottsklubben_Ishockey_Logo.svg.png", thumb: UIImage())]
        self.events += [Event(id: "kff_kalmar", title: "Kalmar FF", desc: "", url: "http://media.24files.se/24kalmar/uploads/2015/04/kalmarff_new.png", thumb: UIImage())]
               return events
    }
*/
    /*
    func returnPosts() -> [Post] {
        self.posts = [Post]()
        self.posts += [Post(id: 1, tag: "kff_kalmar",text: "Post 1", url: "http://media.24files.se/24kalmar/uploads/2015/04/kalmarff_new.png")]
        self.posts += [Post(id: 2, tag: "aik",text: "Post 2", url: "https://scontent.cdninstagram.com/hphotos-xaf1/l/t51.2885-15/s320x320/e35/11910172_179759395688428_370568060_n.jpg")]
        self.posts += [Post(id: 3, tag: "harrys_kalmar" ,text: "Post 3", url: "https://scontent.cdninstagram.com/hphotos-xpt1/t51.2885-15/s320x320/e15/11093084_380799228789159_286870184_n.jpg")]
        self.posts += [Post(id: 4, tag: "harrys_kalmar" ,text: "Post 3", url: "https://scontent.cdninstagram.com/hphotos-xpa1/t51.2885-15/s306x306/e15/10449039_391225704392618_1288843900_n.jpg")]
        self.posts += [Post(id: 5, tag: "harrys_kalmar" ,text: "Post 3", url: "http://images.citybreakcdn.com/image.aspx?ImageId=173469&width=300&height=300&crop=1")]
        
        return posts
    }
*/

    
}