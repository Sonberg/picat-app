//
//  PostListedInGridView.swift
//  Picat
//
//  Created by Per Sonberg on 2016-01-26.
//  Copyright © 2016 Per Sonberg. All rights reserved.
//

import UIKit
import Alamofire
import Foundation
import Kingfisher
import UIScrollView_InfiniteScroll

class PostListedInGridView: PostViewController, UICollectionViewDataSource, UICollectionViewDelegate {
    
    var refreshControl : UIRefreshControl = UIRefreshControl()
    var collectionViewLayout: CustomImageFlowLayout!
    var selectedIndexIdentifier : Int?
    var recivedPosts = [Post]()
    var post : Post?
    
    // MARK: Outlets
    @IBOutlet weak var collectionView: UICollectionView!
    
    // Update on Appear
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        self.collectionView.reloadData()
    }
    
    
    // Load och setup
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(PostListedInGridView.listRecived(_:)),name:"reload", object: nil)
        self.refreshControl.addTarget(self, action: #selector(PostListedInGridView.refreshData(_:)), forControlEvents: UIControlEvents.ValueChanged)
        
        self.collectionView.allowsMultipleSelection = false
        self.definesPresentationContext = true
        self.automaticallyAdjustsScrollViewInsets = true
        self.collectionView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
        collectionViewLayout = CustomImageFlowLayout()
        self.collectionView.collectionViewLayout = collectionViewLayout
        self.refreshControl.backgroundColor = UIColor.whiteColor()
        self.refreshControl.tintColor = Settings.sharedManager.darkYellow
        self.collectionView.alwaysBounceVertical = true;
        self.collectionView.addSubview(refreshControl)

    }
    
    // Clear memory
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        let cache = KingfisherManager.sharedManager.cache
        cache.cleanExpiredDiskCache()
    }

    
    // Refresh data
    func refreshData(sender: AnyObject) {
        NSNotificationCenter.defaultCenter().postNotificationName("refresh", object: nil)
        self.refreshControl.endRefreshing()
    }
    
    func listRecived(notification: NSNotification){
        self.recivedPosts = [Post]()
        for p in notification.userInfo!["json"] as! NSArray {
            self.recivedPosts += [Post(
                id: (p.valueForKey("id") as? Int),
                owner_event: (p.valueForKey("owner_event") as? Int),
                text: (p.valueForKey("text") as? String),
                url: (p.valueForKey("url") as? String),
                likes: (p.valueForKey("likes") as? Int),
                liked : (p.valueForKey("liked") as? Int)!,
                created_at : (p.valueForKey("created_at") as? String)!
                )]
        }
        self.collectionView.reloadData()
        print(self.recivedPosts.count)
    }
    
    
    // MARK: Grid view
    
    /*
    
    Number of sections
    
    */
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    /*
    
    Number of posts
    
    */
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.recivedPosts.count
    }
    
    /*
    
    Give value to fields
    
    */
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cellGrid = collectionView.dequeueReusableCellWithReuseIdentifier("gridPost", forIndexPath: indexPath) as UICollectionViewCell
        
        let post = self.recivedPosts[indexPath.row]
        let imageView = cellGrid.viewWithTag(2) as! UIImageView
        
        imageView.image = UIImage(named: "placeholder")
        let progress = cellGrid.viewWithTag(1) as! UIProgressView
        progress.progress = Float(0)
        progress.alpha = 1
        
        imageView.kf_setImageWithURL(NSURL(string: post.url!)!,
                                     placeholderImage: UIImage(named: "event-placeholder"),
                                     optionsInfo: nil,
                                     progressBlock: { (receivedSize, totalSize) -> () in
                                        progress.progress = Float(receivedSize) / Float(totalSize)
            },
                                     completionHandler: { (image, error, cacheType, imageURL) -> () in
                                        imageView.image = image
                                        progress.alpha = 0
            }
        )
        return cellGrid
    }
    
    /*
    
    Post is clicked
    
    */
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        self.post = self.recivedPosts[indexPath.row]
        //self.selectedIndexIdentifier = (post!.id as! NSString).integerValue
        performSegueWithIdentifier("SingelView", sender: self)
        self.collectionView.deselectItemAtIndexPath(indexPath, animated: false)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "SingelView" {
            let VC = segue.destinationViewController as! SingelTableViewController
            VC.recivedPost = self.post
            VC.postType = self.EventType!
        }
    }

}
