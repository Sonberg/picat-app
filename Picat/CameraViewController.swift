//
//  CameraViewController.swift
//  Picat
//
//  Created by Per Sonberg on 2016-01-07.
//  Copyright © 2016 Per Sonberg. All rights reserved.
//

import UIKit
import SimpleAlert
import FBSDKCoreKit
import FBSDKLoginKit
import AVFoundation
import CoreGraphics

class CameraViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    var FBData : AnyObject?
    var scaleFactor: CGFloat!
    var captureSession : AVCaptureSession?
    var stillImageOutput : AVCaptureStillImageOutput?
    var previewLayer : AVCaptureVideoPreviewLayer?
    var frontCamera : AVCaptureDevice?
    var backCamera : AVCaptureDevice?
    var currentCamera : AVCaptureDevice?
    
    var inputFront : AVCaptureDeviceInput?
    var inputBack : AVCaptureDeviceInput?
    
    var tempImage : UIImage?

    
    // Hide Status bar
    override func prefersStatusBarHidden() -> Bool {
        return true;
    }
    
    // MARK: Outlet
    @IBOutlet weak var CameraView: UIView!
    @IBOutlet weak var TakePhotoButton: UIButton!
    @IBOutlet weak var flashIcon: UIButton!
    @IBOutlet weak var BlackView: UIImageView!
    @IBOutlet weak var eventBrowseButton: UIButton!
    
    @IBAction func ShowPostsButtonPressed(sender: AnyObject) {
        self.performSegueWithIdentifier("ShowPosts", sender: nil)
        self.BlackView.hidden = false;
    }
    
    // MARK: FLASH
    @IBAction func ToggleFlash(sender: AnyObject) {
        var device : AVCaptureDevice?
        if self.currentCamera == nil {
            device = AVCaptureDevice.defaultDeviceWithMediaType(AVMediaTypeVideo)
        } else {
            device = self.currentCamera!
        }
        
        if (device!.flashAvailable) {
            do {
                try device!.lockForConfiguration()
                if (device!.flashMode == AVCaptureFlashMode.Auto || device!.flashMode == AVCaptureFlashMode.On) {
                    device!.flashMode = AVCaptureFlashMode.Off
                    self.flashIcon.setBackgroundImage(UIImage(named: "flash-off"), forState: UIControlState.Normal)
                } else {
                    self.flashIcon.setBackgroundImage(UIImage(named: "flash"), forState: UIControlState.Normal)
                    device!.flashMode = AVCaptureFlashMode.On
                }
                device!.unlockForConfiguration()
            } catch {
                print(error)
            }
        }
    }
    
    func flashAvalibleForCurrentCamera() {
        if (self.currentCamera!.flashAvailable) {
            self.flashIcon.enabled = true
        } else {
            self.flashIcon.enabled = false
        }
    
    }
    
    // MARK: Toggle front/rear camera
    @IBAction func doubleTap(sender: AnyObject) {
        dispatch_async(dispatch_get_main_queue()) {
            if (self.captureSession?.inputs[0])! as! NSObject == self.inputBack! {
                self.switchCamera(self.inputBack!, new: self.inputFront!, current : self.frontCamera!)
            } else {
                self.switchCamera(self.inputFront!, new: self.inputBack!, current : self.backCamera!)
            }
        }
    }
    
    @IBAction func SwitchCameraButtonPressed(sender: AnyObject) {
        if (captureSession?.inputs[0])! as! NSObject == self.inputBack! {
            self.switchCamera(self.inputBack!, new: self.inputFront!, current : self.frontCamera!)
        } else {
            self.switchCamera(self.inputFront!, new: self.inputBack!, current : self.backCamera!)
        }

        
    }
    
    // Photo taken
    @IBAction func TakePhotoButtonPressed(sender: AnyObject) { self.didPressTakePhoto() }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Get scale factor
        scaleFactor = UIScreen.mainScreen().scale
        
        // Rounden corners Photo button
        self.TakePhotoButton.layer.cornerRadius = 0.5 * self.TakePhotoButton.bounds.size.width
        
        // Init current camera
        self.currentCamera = self.backCamera
        
        // Append Overlay
        let overLayViews = OverlayCropToSquare.sharedManager.createOverlay(1)
        self.view.addSubview(overLayViews.0)
        self.view.insertSubview(overLayViews.0, atIndex: 2)
        
        self.view.addSubview(overLayViews.1)
        self.view.insertSubview(overLayViews.1, atIndex: 2)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        PicatService.sharedManager.loginServer(self.FBData!, completion: { (access) in
            if access {
                print("inloggad")
            } else {
                // User blocked
                FBSDKAccessToken.setCurrentAccessToken(nil)
                let loginManager = FBSDKLoginManager()
                loginManager.logOut()
                self.performSegueWithIdentifier("FailSegue", sender: nil)
            }
        })
        
        // Updating from server
        LocationManager.sharedManager.getUsersCurrentLocation()
        
        previewLayer?.frame = CameraView.bounds
        
        captureSession = AVCaptureSession()
        
        if captureSession!.canSetSessionPreset(AVCaptureSessionPresetPhoto) {
            captureSession?.sessionPreset = AVCaptureSessionPresetPhoto
        }
        
        //AVCaptureSessionPresetPhoto
        //AVCaptureSessionPresetHigh
        
        // IF No Camera Can Be Located
        if AVCaptureDevice.devicesWithMediaType(AVMediaTypeVideo).count == 0 {
            let alertController = UIAlertController(title: "Could not find any camera", message:
                "Please restart the app", preferredStyle: UIAlertControllerStyle.Alert)
            alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default,handler: nil))
            
            self.presentViewController(alertController, animated: true, completion: nil)
        }
        
        // Locating Front And Back Camera
        for device in AVCaptureDevice.devicesWithMediaType(AVMediaTypeVideo){
            let device = device as! AVCaptureDevice
            if device.position == AVCaptureDevicePosition.Front {
                self.frontCamera = device
            } else if(device.position == AVCaptureDevicePosition.Back) {
                self.backCamera = device
            }
        }
        do {
            self.inputFront = try AVCaptureDeviceInput(device: frontCamera)
            self.inputBack = try AVCaptureDeviceInput(device: backCamera)
            
            if ((captureSession?.canAddInput(inputBack)) != nil) {
                captureSession?.addInput(inputBack)
                //self.currentCamera = frontCamera
                
                stillImageOutput = AVCaptureStillImageOutput()
                stillImageOutput?.outputSettings = [AVVideoCodecKey : AVVideoCodecJPEG]
                
                if ((captureSession?.canAddOutput(stillImageOutput)) != nil) {
                    captureSession?.addOutput(stillImageOutput)
                    
                    
                    previewLayer = AVCaptureVideoPreviewLayer(session: captureSession!)
                    previewLayer?.frame = self.CameraView.bounds
                    previewLayer?.videoGravity = AVLayerVideoGravityResizeAspect
                    //AVLayerVideoGravityResizeAspect
                    previewLayer?.connection.videoOrientation = AVCaptureVideoOrientation.Portrait
                    
                    self.CameraView.layer.addSublayer(previewLayer!)
                    self.captureSession?.startRunning()
                    
                    //if currentCamera != nil { self.flashAvalibleForCurrentCamera()}
                    
                    // Remove black layer
                    let delay = 0.8 * Double(NSEC_PER_SEC)
                    let time = dispatch_time(DISPATCH_TIME_NOW, Int64(delay))
                    dispatch_after(time, dispatch_get_main_queue()) {
                        self.BlackView.hidden = true;
                    }
                }
            }
            
        } catch { print(error) }
    }
    
    // MARK: Switch Camera
    func switchCamera(old : AVCaptureDeviceInput, new : AVCaptureDeviceInput, current : AVCaptureDevice) {
        self.captureSession?.beginConfiguration()
        self.captureSession?.removeInput(old)
        self.captureSession?.addInput(new)
        self.captureSession?.commitConfiguration()
        
        self.currentCamera = current
        
        // Does the current camera have a flash
        self.flashAvalibleForCurrentCamera()
        
    }
    
    // Take Photo
    func didPressTakePhoto() {
        if let videoConnection = stillImageOutput?.connectionWithMediaType(AVMediaTypeVideo) {
            videoConnection.videoOrientation = AVCaptureVideoOrientation.Portrait
            stillImageOutput?.captureStillImageAsynchronouslyFromConnection(videoConnection, completionHandler: {
                (sampleBuffer, error) in
                
                if sampleBuffer != nil {
                    let imageData = AVCaptureStillImageOutput.jpegStillImageNSDataRepresentation(sampleBuffer)
                    let dataProvider = CGDataProviderCreateWithCFData(imageData)
                    let cgImageRef = CGImageCreateWithJPEGDataProvider(dataProvider, nil, true, .RenderingIntentDefault)
                    
                    // How should the image be renderd
                    if ((self.captureSession?.inputs[0])! as! NSObject == self.inputBack!)
                    {
                       self.tempImage = UIImage(CGImage: cgImageRef!, scale: self.scaleFactor, orientation: UIImageOrientation.Right)
                    } else {
                        self.tempImage = UIImage(CGImage: cgImageRef!, scale: self.scaleFactor, orientation: UIImageOrientation.LeftMirrored)
                    }
                    
                    self.captureSession?.stopRunning()
                    
                    self.performSegueWithIdentifier("ImageSegue", sender: nil)
                    self.BlackView.hidden = false;
                    
                }
                
            })
        }
    }
    
    
    // MARK: Send Image To Next View
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "ImageSegue" {
            let DestViewController : ImageViewController = segue.destinationViewController as! ImageViewController
            DestViewController.RecivedImage = self.tempImage
        }
    }
}

extension AVCaptureVideoOrientation {
    var uiInterfaceOrientation: UIInterfaceOrientation {
        get {
            switch self {
            case .LandscapeLeft:        return .LandscapeLeft
            case .LandscapeRight:       return .LandscapeRight
            case .Portrait:             return .Portrait
            case .PortraitUpsideDown:   return .PortraitUpsideDown
            }
        }
    }
    
    init(ui:UIInterfaceOrientation) {
        switch ui {
        case .LandscapeRight:       self = .LandscapeRight
        case .LandscapeLeft:        self = .LandscapeLeft
        case .Portrait:             self = .Portrait
        case .PortraitUpsideDown:   self = .PortraitUpsideDown
        default:                    self = .Portrait
        }
    }
}
