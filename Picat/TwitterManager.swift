//
//  TwitterManager.swift
//  Picat
//
//  Created by Per Sonberg on 2016-01-20.
//  Copyright © 2016 Per Sonberg. All rights reserved.
//

import Foundation
import UIKit
import Social
import Accounts

class TwitterManager: NSObject {
    let StatusTitle : String = "Error"
    let StatusMessage: String = "Tweet not sent"
    
    let AlertTitle : String = "No account set"
    let AlertMessage : String = "You need to be login to your twitter account"
    
    
    class var sharedManager: TwitterManager {
        struct Singleton {
            static let instance = TwitterManager()
        }
        return Singleton.instance
    }
    
    func ServiceAvalible () -> Bool {
        return SLComposeViewController.isAvailableForServiceType(SLServiceTypeTwitter)
    }
    
    func postToTwitter(image: UIImage, caption: String) {
        if ServiceAvalible() {
            let account = ACAccountStore()
            let accountType = account.accountTypeWithAccountTypeIdentifier(ACAccountTypeIdentifierTwitter)
            account.requestAccessToAccountsWithType(accountType, options: nil, completion: {(granted: Bool, error: NSError!) -> Void in
                if granted {
                    let arrayOfAccounts =
                    account.accountsWithAccountType(accountType)
                    if arrayOfAccounts.count > 0 {
                        let twitterAccount = arrayOfAccounts.first as! ACAccount
                        var message = Dictionary<String, AnyObject>()
                        message["status"] = caption
            
                        let requestURL = NSURL(string:
                            "https://api.twitter.com/1.1/statuses/update_with_media.json")
                        let postRequest = SLRequest(forServiceType:
                            SLServiceTypeTwitter,
                            requestMethod: SLRequestMethod.POST,
                            URL: requestURL,
                            parameters: message)
            
                        postRequest.account = twitterAccount
                        postRequest.addMultipartData(UIImagePNGRepresentation(ImageUtil.cropToSquare(image: image)), withName: "media", type: nil, filename: nil)
            
                        postRequest.performRequestWithHandler({
                            (responseData: NSData!,
                            urlResponse: NSHTTPURLResponse!,
                            error: NSError!) -> Void in
                            if let err = error {
                                print("Error : \(err.localizedDescription)")
                            }
                            print("Twitter HTTP response \(urlResponse.statusCode)")
                            if urlResponse.statusCode != 200 {UIAlertView(title: self.StatusTitle, message: self.StatusMessage, delegate:nil, cancelButtonTitle:"Ok").show()}
                        })
                    }
                } else {
                    print(error)
                }
            
            })
        } else {
            UIAlertView(title: self.AlertTitle, message: self.AlertMessage, delegate:nil, cancelButtonTitle:"Ok").show()
        }
    }
}