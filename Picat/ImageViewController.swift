//
//  ImageViewController.swift
//  Picat
//
//  Created by Per Sonberg on 2016-01-07.
//  Copyright © 2016 Per Sonberg. All rights reserved.
//

import UIKit
import Social
import AMPopTip
import SwiftLoader
import QuartzCore
import JSSAlertView

class ImageViewController: UIViewController {
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    // Hide Status bar
    override func prefersStatusBarHidden() -> Bool {
        return true;
    }
    
    // MARK: Outlet
    @IBOutlet weak var ImageView: UIImageView!
    @IBOutlet var SwipeRightGesture: UISwipeGestureRecognizer!
    @IBOutlet var SwipeLeftGesture: UISwipeGestureRecognizer!
    @IBOutlet weak var NextButton: UIButton!
    @IBOutlet weak var eventButton: UIButton!
    @IBOutlet weak var timelimitButton: UIButton!
    
    var RecivedImage : UIImage?
    var FilteredImage : UIImage?
    var currentUrl : String = ""
    var watermarkSize : CGFloat = 4
    var watermarks : [String : AnyObject]?
    
    var eventURls = [String]()
    var timelimitURLs = [String]()
    var currentWatermark = [String : String]()
    
    var selectedWatermark : String?
    var selectedOwnerEvent : String?
    
    var screenSize = UIScreen.mainScreen().bounds
    let context = CIContext(options: nil)
    var extent : CGRect!
    var scaleFactor: CGFloat!
    
    //MARK: Define filters
    var currentSwipeHorizontalIndex : Int = 0
    let AvalibleFilterArray = ["reset", "CIPhotoEffectChrome", "CIPhotoEffectTransfer", "CIPhotoEffectNoir", "CIPhotoEffectProcess", "CIPhotoEffectInstant"]
    
    /*
    MARK: Avalible Filters
    @"CIPhotoEffectChrome"
    @"CIPhotoEffectFade"
    @"CIPhotoEffectInstant",
    @"CIPhotoEffectMono"
    @"CIPhotoEffectNoir"
    @"CIPhotoEffectProcess"
    @"CIPhotoEffectTonal"
    @"CIPhotoEffectTransfer"
    */
    
    @IBAction func DiscardPhotoButtonPressed(sender: AnyObject) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    // Change Watermark Size
    @IBAction func scaleDownWatermarkButtonPressed(sender: AnyObject) {
        if self.currentUrl != "" {
            if self.watermarkSize < CGFloat(5) {
                self.watermarkSize += CGFloat(0.2)
                self.applyWatermarkToImage(self.currentUrl)
            }
        }
    }
    
    @IBAction func scaleUpWatermarkButtonPressed(sender: AnyObject) {
        if self.currentUrl != "" {
            if self.watermarkSize > CGFloat(2) {
                self.watermarkSize -= CGFloat(0.2)
                self.applyWatermarkToImage(self.currentUrl)
            }
        }
    }
    
    // Apply Watermark
    @IBAction func timelimitedWatermarkButtonPressed(sender: AnyObject) {
        
        let count = self.timelimitURLs.count
        var index : Int = 0
        
        if self.currentWatermark["type"] == "event" {
            self.applyWatermarkToImage(self.timelimitURLs[0])
            
        } else {
            if count > 1 {
                if Int(self.currentWatermark["index"]!)! > Int(count - 1){
                    index = 0
                } else if Int(self.currentWatermark["index"]!)! < 0 {
                    index = Int(count - 1)
                }
                
                index += 1
                
                if Int(self.currentWatermark["index"]!)! == Int(count - 1) {
                    index -= 1
                }
            }
        }
        self.applyWatermarkToImage(self.timelimitURLs[index])
        self.currentWatermark = ["type" : "timelimit", "index" : String(index)]
        self.currentUrl = self.timelimitURLs[index]
        
    }
    
    @IBAction func saveImageToGallery(sender: AnyObject) {
        SwiftLoader.show(title: "Saving...", animated: true)
        let img = ImageUtil.cropToSquare(image: self.ImageView.image!)
        UIImageWriteToSavedPhotosAlbum(img, self, #selector(ImageViewController.image(_:didFinishSavingWithError:contextInfo:)), nil)    }
    
    @IBAction func currentLocationWatermarkButtonPressed(sender: AnyObject) {
        let count = self.eventURls.count
        var index : Int = 0
        
        if self.currentWatermark["type"] == "timelimit" {
            self.applyWatermarkToImage(self.eventURls[0])
            
        } else {
            if count > 1 {
                if Int(self.currentWatermark["index"]!)! > Int(count - 1){
                    index = 0
                } else if Int(self.currentWatermark["index"]!)! < 0 {
                    index = Int(count - 1)
                }
                
                index += 1
                
                if Int(self.currentWatermark["index"]!)! == Int(count - 1) {
                    index -= 1
                }
            }
        }
        self.applyWatermarkToImage(self.eventURls[index])
        self.currentWatermark = ["type" : "event", "index" : String(index)]
        self.currentUrl = self.eventURls[index]
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Append Overlay
        let overLayViews = OverlayCropToSquare.sharedManager.createOverlay()
        view.addSubview(overLayViews.0)
        view.insertSubview(overLayViews.0, atIndex: 1)
        
        view.addSubview(overLayViews.1)
        view.insertSubview(overLayViews.1, atIndex: 1)

        self.normalizeUIImage()
        
        scaleFactor = UIScreen.mainScreen().scale
        extent = CGRectApplyAffineTransform(UIScreen.mainScreen().bounds, CGAffineTransformMakeScale(scaleFactor, scaleFactor))
        
        // MARK: Swipe Gesture handler
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(ImageViewController.didSwipeLeft(_:)))
        swipeLeft.direction = UISwipeGestureRecognizerDirection.Left
        self.view.addGestureRecognizer(swipeLeft)
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(ImageViewController.didSwipeRight(_:)))
        swipeRight.direction = UISwipeGestureRecognizerDirection.Right
        self.view.addGestureRecognizer(swipeRight)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        if NSUserDefaults.standardUserDefaults().boolForKey("isFirstLaunch") {
            
            NSUserDefaults.standardUserDefaults().setBool(true, forKey: "isFirstLaunch")
            NSUserDefaults.standardUserDefaults().synchronize()
            
            let alert = JSSAlertView().show(self, title: "Swipe to apply filter", color: Settings.sharedManager.blue)
            alert.setTextTheme(JSSAlertView.TextColorTheme.Light)
        } else {
            
        }

        
        // Show help för first time users
        
        // Fetch watermarks from server
        watermarks = PicatService.sharedManager.returnWatermarks()
        
        if (self.watermarks!["event"]!).count > 0 {
            if let events = self.watermarks!["event"] {
                for event in events as! NSArray {
                    self.eventURls.append(event.valueForKey("url") as! String)
                }
            }
        }
        
        if (self.watermarks!["timelimit"]!).count > 0 {
            if let events = self.watermarks!["timelimit"] {
                for event in events as! NSArray {
                    self.timelimitURLs.append(event.valueForKey("url") as! String)
                }
            }
        }
        
        self.checkWatermarks()
    }
    
    func didSwipeLeft(gesture: UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            if swipeGesture.direction == UISwipeGestureRecognizerDirection.Left {
                currentSwipeHorizontalIndex = currentSwipeHorizontalIndex + 1
                self.willChangeFilter()
                
            }
        }
        
    }
    
    func didSwipeRight(gesture: UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            if swipeGesture.direction == UISwipeGestureRecognizerDirection.Right {
                currentSwipeHorizontalIndex = currentSwipeHorizontalIndex - 1
                self.willChangeFilter()
            }
        }
    }
    
    func willChangeFilter() {
        if currentSwipeHorizontalIndex > Int(AvalibleFilterArray.count - 1){
            currentSwipeHorizontalIndex = 0
        } else if currentSwipeHorizontalIndex < 0 {
            currentSwipeHorizontalIndex = Int(AvalibleFilterArray.count - 1)
        }
        
        self.applyFilter(String(AvalibleFilterArray[currentSwipeHorizontalIndex]))
    
    }
    
    func normalizeUIImage() {
        self.ImageView.image = self.RecivedImage!.fixOrientation()
        self.RecivedImage = self.ImageView.image
        self.FilteredImage = self.RecivedImage
    }
    
    func resetImage() {
        self.ImageView.image = self.RecivedImage
        self.FilteredImage = self.RecivedImage
        self.resetWatermarkWhenFilterChange()
    }
    
    func applyFilter(filter : String) {
        if filter == String(AvalibleFilterArray[0]) {
            self.resetImage()
        } else {
            // Create an image to filter
            let inputImage = CIImage(image: self.RecivedImage!)
        
            // Apply a filter to image
            let filteredImage = inputImage!.imageByApplyingFilter(filter, withInputParameters: nil)
        
            // Render the filtered image
            let renderedImage = context.createCGImage(filteredImage, fromRect: filteredImage.extent)
        
            // Reflect change back in interface
            self.ImageView.image = UIImage(CGImage: renderedImage).fixOrientation()
            self.FilteredImage = UIImage(CGImage: renderedImage).fixOrientation()
            self.resetWatermarkWhenFilterChange()
        }
    }
    
    func resetWatermarkWhenFilterChange() {
        if currentUrl != "" {
            applyWatermarkToImage(currentUrl)
        }
    }
    
    func applyWatermarkToImage(url : String = "") {
        
        if url != ""  {
            if url.characters.count > 5 {
                    getImageFromUrl.sharedManager.fetch(NSURL(string: url)!, progress: nil, completionHandler: { (succeeded, image) -> Void in
                        dispatch_async(dispatch_get_main_queue()) {
                           // self.curretWatermark = watermark
                            let backgroundImage = self.FilteredImage
                            let squarePosition = ((backgroundImage?.size.height)! - (backgroundImage?.size.width)!) / 2
                            
                            let watermarkImage : UIImage = image!
                            let watermarkWidth : CGFloat = (backgroundImage!.size.width) / self.watermarkSize
                            let watermarkHeight : CGFloat = (watermarkImage.size.height / watermarkImage.size.width) * watermarkWidth
                            
                            UIGraphicsBeginImageContext(backgroundImage!.size)
                            backgroundImage!.drawInRect(CGRectMake(0,0,
                                backgroundImage!.size.width,
                                backgroundImage!.size.height))
                            //(100*self.scaleFactor)
                            watermarkImage.drawInRect(CGRectMake(10, (squarePosition + (backgroundImage?.size.width)! - watermarkHeight) - 10, watermarkWidth, watermarkHeight))
                            
                            let result : UIImage = UIGraphicsGetImageFromCurrentImageContext()
                            UIGraphicsEndImageContext()
                            
                            self.ImageView.image = result
                            
                           // self.saveOwner(watermark, stamp: self.watermarks![watermark]!)
                        }
                    })
                }
            }
    }
    
    func checkWatermarks() {
        if self.eventURls.count > 0 {
            self.eventButton.enabled = true
            self.applyWatermarkToImage(self.eventURls[0])
            self.currentWatermark = ["type" : "event" ,"index" : "0"]
            self.currentUrl = self.eventURls[0]
        } else {
            
            let popTip = AMPopTip()
            popTip.popoverColor = Settings.sharedManager.darkYellow
            popTip.actionAnimation = AMPopTipActionAnimation.Float
            popTip.arrowSize = CGSize(width: CGFloat(10), height: CGFloat(10))
            popTip.showText("No events nearby", direction: AMPopTipDirection.Up, maxWidth: CGFloat(200), inView: self.view!, fromFrame: self.eventButton.frame)
            
            if self.timelimitURLs.count > 0 {
                self.applyWatermarkToImage(self.timelimitURLs[0])
                self.currentWatermark = ["type" : "timelimit","index" : "0"]
                self.currentUrl = self.timelimitURLs[0]
            }
        }
        
        if self.timelimitURLs.count > 0 {
            self.timelimitButton.enabled = true
        }
        
        if self.timelimitURLs.count == 0 && self.eventURls.count == 0 {
            self.NextButton.enabled = false
            let alert = JSSAlertView().show(
                self,
                title: "Sorry, no event available here yet!",
                color: Settings.sharedManager.blue
            )
            alert.setTextTheme(JSSAlertView.TextColorTheme.Light)
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "ShareImageSegue" {
            let DestViewController : ShareImageViewController = segue.destinationViewController as! ShareImageViewController
            DestViewController.RecivedImage = self.ImageView.image!
            if let owner = self.watermarks![(self.currentWatermark["type"])!] {
                let index = Int(self.currentWatermark["index"]!)
                
                if self.currentWatermark["type"] == "timelimit" {
                    if owner is NSArray {
                        DestViewController.ownerEvent = String((owner as! NSArray).objectAtIndex(index!).valueForKey("id")!)
                    }
                    if owner is NSDictionary {
                        DestViewController.ownerEvent = String((owner as! NSDictionary).valueForKey("id"))
                    }
                } else {
                    DestViewController.ownerEvent = String((owner as! NSArray).objectAtIndex(index!).valueForKey("owner_event")!)
                }
            }
            DestViewController.eventType = self.currentWatermark["type"]
        }
    }
    
    func image(image: UIImage, didFinishSavingWithError error: NSError?, contextInfo:UnsafePointer<Void>) {
        SwiftLoader.hide()
        if error == nil {
            
        } else {
            let alert = JSSAlertView().show(
                self,
                title: "Sorry, there was an error!",
                color: Settings.sharedManager.blue
            )
            alert.setTextTheme(JSSAlertView.TextColorTheme.Light)
        }
    }
}
