//
//  Event.swift
//  Picat
//
//  Created by Per Sonberg on 2016-01-11.
//  Copyright © 2016 Per Sonberg. All rights reserved.
//

import Foundation
import UIKit

struct Event {
    var id : String
    var title : String
    var desc : String?
    var thumb : String
    var distance : String?
    
    var owner_company : String?
    
    var event_type : String?
    var date_start : String?
    var date_stop : String?
    
    var latitude : Double?
    var longitude : Double?
}
