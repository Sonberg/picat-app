//
//  EventsViewController.swift
//  Picat
//
//  Created by Per Sonberg on 2016-01-11.
//  Copyright © 2016 Per Sonberg. All rights reserved.
//

import UIKit
import Foundation
import Kingfisher

class EventsViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UISearchResultsUpdating, UISearchBarDelegate, UISearchControllerDelegate {
    
    // MARK: - Init Settings
    var type : PicatService.viewType?
    
    var resultSearchController : UISearchController = UISearchController()
    var refreshControl : UIRefreshControl = UIRefreshControl()
    var collectionViewLayout: CustomImageFlowLayout!
    var selectedIndexIdentifier : String?
    var location : (String, String)?
    var filteredEvents = [Event]()
    var events = [Event]()
    var screenSize : CGSize?
    
    
    // MARK : Outlets
    @IBOutlet weak var ContainerGridView: UIView!
    @IBOutlet weak var CollectionView: UICollectionView!
    @IBOutlet weak var searchBarPlaceholder: UIView!
    
    // MARK: Action
    @IBAction func CloseViewButtonPressed(sender: AnyObject) {
        dismissViewControllerAnimated(false, completion: nil)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        self.navigationController?.navigationBarHidden = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.filteredEvents = self.events
        self.CollectionView.allowsMultipleSelection = false
        collectionViewLayout = CustomImageFlowLayout()
        self.CollectionView.collectionViewLayout = collectionViewLayout
        
        self.resultSearchController = UISearchController(searchResultsController: nil)
        self.resultSearchController.delegate = self
        self.resultSearchController.searchResultsUpdater = self
        self.resultSearchController.dimsBackgroundDuringPresentation = false
        self.resultSearchController.searchBar.sizeToFit()
        self.resultSearchController.hidesNavigationBarDuringPresentation = false;
        self.resultSearchController.searchBar.searchFieldBackgroundPositionAdjustment.vertical.distanceTo(CGFloat(5))
        self.definesPresentationContext = false
        self.resultSearchController.searchBar.layer.borderColor = Settings.sharedManager.black.CGColor
        automaticallyAdjustsScrollViewInsets = true
        self.resultSearchController.searchResultsUpdater = self
        self.resultSearchController.loadViewIfNeeded()
        
        
        // Text color
        self.resultSearchController.searchBar.tintColor = UIColor.blackColor()
        
        // Background color
        self.resultSearchController.searchBar.barTintColor = UIColor.whiteColor()
        //self.resultSearchController.searchBar.clipsToBounds = false
        self.searchBarPlaceholder.addSubview(self.resultSearchController.searchBar)
        
        self.refreshControl.backgroundColor = UIColor.whiteColor()
        self.refreshControl.tintColor = Settings.sharedManager.darkYellow
        self.refreshControl.addTarget(self, action: #selector(EventsViewController.refreshData(_:)), forControlEvents: UIControlEvents.ValueChanged)
        self.CollectionView.alwaysBounceVertical = true;
        self.CollectionView.addSubview(refreshControl)

         self.prepearData(PicatService.sharedManager.returnEvents(type!))
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        let cache = KingfisherManager.sharedManager.cache
        cache.cleanExpiredDiskCache()
    }
    
    
    func refreshData(sender: AnyObject) {
        PicatService.sharedManager.fetch(type!)
        self.prepearData(PicatService.sharedManager.returnEvents(type!))
        self.refreshControl.endRefreshing()
    }
    
    // Prepear data
    
    func prepearData(data : [String : AnyObject]) {
        self.events = [Event]()
        
        switch type! {
        case .timelimit:
            for a in (data["all"]) as! NSArray {
                let thumb : () -> String  = {
                    return (a.valueForKey("url") as! String)
                    /*
                    if a.valueForKey("thumb") != nil {
                        return (a.valueForKey("thumb") as! String)
                    } else {
                        return (a.valueForKey("url") as! String)
                    }
                     */
                }
                self.events += [Event(
                    id: (a.valueForKey("id") as! String),
                    title: (a.valueForKey("title") as! String),
                    desc: String(0),
                    thumb: thumb(),
                    distance : String(0),
                    owner_company : String(0),
                    event_type : String(0),
                    date_start : (a.valueForKey("date_start") as? String),
                    date_stop : (a.valueForKey("date_stop") as? String),
                    latitude : (0),
                    longitude : (0)
                    )]
            }
            break
            
        case .student:
            for a in (data["all"]) as! NSArray {
                self.events += [Event(
                    id: (a.valueForKey("id") as! String),
                    title: (a.valueForKey("title") as! String),
                    desc: (a.valueForKey("desc") as? String),
                    thumb: (a.valueForKey("thumb") as! String),
                    distance : String(0),
                    owner_company : (a.valueForKey("owner_company") as! String),
                    event_type : String(0),
                    date_start : String(0),
                    date_stop : String(0),
                    latitude : (a.valueForKey("latitude") as? NSString)!.doubleValue,
                    longitude : (a.valueForKey("longitude") as? NSString)!.doubleValue
                    )]
            }
            break
            
        case .event:
            for a in (data["all"]) as! NSArray {
                self.events += [Event(
                    id: (a.valueForKey("id") as! String),
                    title: (a.valueForKey("title") as! String),
                    desc: (a.valueForKey("desc") as? String),
                    thumb: (a.valueForKey("thumb") as! String),
                    distance : (a.valueForKey("distance") as? String),
                    owner_company : (a.valueForKey("owner_company") as! String),
                    event_type : (a.valueForKey("event_type") as? String),
                    date_start : (a.valueForKey("date_start") as? String),
                    date_stop : (a.valueForKey("date_stop") as? String),
                    latitude : (a.valueForKey("latitude") as? NSString)!.doubleValue,
                    longitude : (a.valueForKey("longitude") as? NSString)!.doubleValue
                    )]
            }
            break
            
        case .all:
            break
        }
        
        self.CollectionView.reloadData()
    }
    
    // MARK: Grid view
    
    /*
    
    Number of sections
    
    */
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    /*
    
    Number of posts
    
    */
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.resultSearchController.active && self.resultSearchController.searchBar.text?.characters.count != 0 {
            return filteredEvents.count
        } else {
            if events.count > 20 {
                return 20
            } else {
                return events.count
            }
        }
    }
    
    /*
    
    Give value to fields
    
    */
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cellGrid = collectionView.dequeueReusableCellWithReuseIdentifier("grid", forIndexPath: indexPath) as UICollectionViewCell
        cellGrid.backgroundColor = Settings.sharedManager.lightgrey
        
        var event : Event?
        if self.resultSearchController.active && self.resultSearchController.searchBar.text?.characters.count != 0 {
            event = filteredEvents[indexPath.row]
        } else {
            event = events[indexPath.row]
        }
        
        let imageView = cellGrid.viewWithTag(2) as! UIImageView
        let progress = cellGrid.viewWithTag(1) as! UIProgressView
        progress.progress = Float(0)
        progress.alpha = 1
        
        imageView.kf_setImageWithURL(NSURL(string: event!.thumb)!,
                                     placeholderImage: UIImage(named: "event-placeholder"),
                                     optionsInfo: nil,
                                     progressBlock: { (receivedSize, totalSize) -> () in
                                        progress.progress = Float(receivedSize) / Float(totalSize)
                                    },
                                     completionHandler: { (image, error, cacheType, imageURL) -> () in
                                        imageView.image = image
                                        progress.alpha = 0
                                    }
        )
        return cellGrid
    }
    
    /*
    
    Post is clicked
    
    */
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        var event : Event?
        if self.resultSearchController.active && self.resultSearchController.searchBar.text?.characters.count != 0 {
            event = filteredEvents[indexPath.row]
        } else {
            event = events[indexPath.row]
        }
        
        self.selectedIndexIdentifier = event!.id
        performSegueWithIdentifier("ShowPostsFromEventSegue", sender: nil)
        //self.CollectionView.deselectItemAtIndexPath(indexPath, animated: false)
    }
    
    // MARK: Search events
    
    /*
    
    Show cancel button
    
    */
    func didPresentSearchController(searchController: UISearchController) {
        searchController.searchBar.showsCancelButton = true
    }
    
    /*
    
    Filter matching posts
    
    */
    func filterContentForSearchText(searchText: String, scope: String = "All") {
        self.filteredEvents = self.events.filter { event in
            return event.title.lowercaseString.containsString(searchText.lowercaseString)
        }
        CollectionView.reloadData()
    }
    
    /*
    
    Search text is updated
    
    */
    func updateSearchResultsForSearchController(searchController: UISearchController) {
        filterContentForSearchText(self.resultSearchController.searchBar.text!, scope: "All")
    }
    
    // MARK: Send data with segue
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        // Remove text from back button
        let backItem = UIBarButtonItem()
        backItem.title = ""
        navigationItem.backBarButtonItem = backItem
        
        if segue.identifier == "ShowPostsFromEventSegue" {
            let DestViewController : PostViewController = segue.destinationViewController as! PostViewController
            DestViewController.RecivedIdentifier = self.selectedIndexIdentifier
            
            switch type! {
            case .timelimit:
                DestViewController.EventType = "timelimit"
                break
            default:
                DestViewController.EventType = "event"
                break
            }
            
        }
    }
    
}
