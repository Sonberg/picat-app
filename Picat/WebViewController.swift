//
//  WebViewController.swift
//  Picat
//
//  Created by Per Sonberg on 2016-04-05.
//  Copyright © 2016 Per Sonberg. All rights reserved.
//

import UIKit

class WebViewController: UIViewController, UIWebViewDelegate {
    
    var pageTitle : String = ""
    var url : String = ""
    
    // MARK: - Outlets
    @IBOutlet weak var webView: UIWebView!
    
    @IBAction func dismissButtonPressed(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    // Hide Status bar
    override func prefersStatusBarHidden() -> Bool {
        return true;
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.title = pageTitle
        
        let nurl = NSURL (string: self.url);
        let requestObj = NSURLRequest(URL: nurl!);
        self.webView.loadRequest(requestObj);
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }
   
}
