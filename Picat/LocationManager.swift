//
//  LocationManager.swift
//  Picat
//
//  Created by Per Sonberg on 2016-01-21.
//  Copyright © 2016 Per Sonberg. All rights reserved.
//

import Foundation
import CoreLocation

class LocationManager: NSObject, CLLocationManagerDelegate {
    
    // singleton manager
    class var sharedManager: LocationManager {
        struct Singleton {
            static let instance = LocationManager()
        }
        return Singleton.instance
    }
    // MARK: Model classes
    let locationManager = CLLocationManager()
    var lat : String?
    var long : String?
    
    // MARK: Location Delegate
    func getUsersCurrentLocation() {
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
        self.locationManager.distanceFilter = 1
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.startUpdatingLocation()
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations.last
        self.lat = String(location!.coordinate.latitude)
        self.long = String(location!.coordinate.longitude)
        self.locationManager.stopUpdatingLocation()
        PicatService.sharedManager.fetchWatermarks(String(location!.coordinate.latitude), long: String(location!.coordinate.longitude))
        PicatService.sharedManager.fetch(PicatService.viewType.all)
    }
    
    // MARK: - Location Error
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        print("Errors: " + error.localizedDescription)
    }
    
    func returnLocation() -> (lat : String, long : String) {
        if lat == nil { return ("no", "no")}
        return (self.lat!, self.long!)
    }
    
}