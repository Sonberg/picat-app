//
//  SignupViewController.swift
//  Picat
//
//  Created by Per Sonberg on 2016-01-15.
//  Copyright © 2016 Per Sonberg. All rights reserved.
//

import UIKit
import SimpleAlert
import FBSDKCoreKit
import FBSDKLoginKit
import VideoSplashKit

class SignupViewController: VideoSplashViewController, FBSDKLoginButtonDelegate {
    
    var data : NSDictionary = NSDictionary()
    
    // MARK: - Outlet
    @IBOutlet weak var outlineLogo: UIImageView!
    
    // Only allow portrait orientaion
    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.Portrait
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    
        if (FBSDKAccessToken.currentAccessToken() != nil) {
            self.LoginWithFacebook()
        } else {
            // Basic Setup
            outlineLogo.alpha = 1
            self.setupFB()
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        LocationManager.sharedManager.getUsersCurrentLocation()
        if FBSDKAccessToken.currentAccessToken() == nil {
            self.setupVideoBackground()
        }
    }
    
    
    func loginButton(loginButton: FBSDKLoginButton!, didCompleteWithResult result: FBSDKLoginManagerLoginResult!, error: NSError!) {
        if error == nil {
            if FBSDKAccessToken.currentAccessToken() != nil {
                self.LoginWithFacebook()
            }
        } else { print(error.localizedDescription)}
    }
    
    func loginButtonDidLogOut(loginButton: FBSDKLoginButton!) {
        
    }
    
    func calcFBButtonPos() -> CGPoint {
        let screen = UIScreen.mainScreen().bounds
        let center = self.view.center
        let pos : CGPoint = CGPoint(x: center.x, y: screen.height - 150)
        return pos
    
    }
    
    func LoginWithFacebook() {
        let req = FBSDKGraphRequest(graphPath: "me", parameters: ["fields":"email,name,picture"])
        req.startWithCompletionHandler({ (connection, result, error : NSError!) -> Void in
            if(error == nil) {
                if(result is NSDictionary) {
                    self.data = result as! NSDictionary
                }
                
                if(result is NSArray) {
                    self.data = (result as! NSArray).firstObject as! NSDictionary
                }
                
                self.performSegueWithIdentifier("ReadySegue", sender: nil)
            } else { print("error \(error)") }
        })
    }
    
    // Set up and add background video to screen
    func setupVideoBackground() -> Void {
        var url : NSURL?
        
        if let path = NSBundle.mainBundle().pathForResource("splash", ofType: "mp4") {
            url = NSURL(fileURLWithPath: path)
        }
        
        self.videoFrame = self.view.frame
        self.fillMode = .ResizeAspectFill
        self.alwaysRepeat = true
        self.sound = false
        self.startTime = 0.0
        self.duration = 6.15
        self.alpha = 0.7
        self.backgroundColor = UIColor.blackColor()
        if url != nil { self.contentURL = url! } else { print("error") }
        self.restartForeground = true
    }
    
    func setupFB() {
        let loginButton = FBSDKLoginButton()
        loginButton.readPermissions = ["public_profile", "email", "user_friends"]
        loginButton.publishPermissions = ["publish_actions"]
        loginButton.center = calcFBButtonPos()
        loginButton.delegate = self
        
        self.view.addSubview(loginButton)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "ReadySegue" {
         let vc = segue.destinationViewController as! CameraViewController
         vc.FBData = self.data
        }
        
        if segue.identifier == "webViewSeque" {
            let VC = segue.destinationViewController as! WebViewController
            VC.pageTitle = "Terms of use (EULA)"
            VC.url = "https://sheltered-lowlands-88850.herokuapp.com/policy"
        }
    }

}

