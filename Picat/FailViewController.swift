//
//  FailViewController.swift
//  Picat
//
//  Created by Per Sonberg on 2016-04-17.
//  Copyright © 2016 Per Sonberg. All rights reserved.
//

import UIKit
import MessageUI

class FailViewController: UIViewController, MFMailComposeViewControllerDelegate {
    
    @IBAction func contactButtonPressed(sender: AnyObject) {
        sendEmail()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func sendEmail() {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients([Settings.sharedManager.issueMail])
            mail.setSubject("Blocked account")
            
            presentViewController(mail, animated: true, completion: nil)
        } else {
            // show failure alert
        }
    }
    
    func mailComposeController(controller: MFMailComposeViewController, didFinishWithResult result: MFMailComposeResult, error: NSError?) {
        controller.dismissViewControllerAnimated(true, completion: nil)
    }
}
