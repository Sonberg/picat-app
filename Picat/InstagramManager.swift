import Foundation
import UIKit


class InstagramManager: NSObject, UIDocumentInteractionControllerDelegate {
    let documentInteractionController : UIDocumentInteractionController = UIDocumentInteractionController()
    
    private let kInstagramURL = NSURL(string: "instagram://")
    private let kUTI = "com.instagram.exclusivegram"
    private let kfileNameExtension = "instagram.igo"
    private let kAlertViewTitle = "Error"
    private let kAlertViewMessage = "Please install the Instagram application"
    
    // singleton manager
    class var sharedManager: InstagramManager {
        struct Singleton {
            static let instance = InstagramManager()
        }
        return Singleton.instance
    }
    
    func ServiceAvalible() -> Bool {
        return UIApplication.sharedApplication().canOpenURL(kInstagramURL!)
    }
    
    func postImageToInstagramWithCaption(imageInstagram: UIImage, instagramCaption: String, view: UIView) {
        // called to post image with caption to the instagram application
        
        if ServiceAvalible() {
            let jpgPath = (NSTemporaryDirectory() as NSString).stringByAppendingPathComponent(kfileNameExtension)
            UIImageJPEGRepresentation(imageInstagram, 1.0)!.writeToFile(jpgPath, atomically: true)
            let rect = CGRectZero
            let fileURL = NSURL.fileURLWithPath(jpgPath)
            self.documentInteractionController.URL = fileURL
            self.documentInteractionController.delegate = self
            self.documentInteractionController.UTI = kUTI
            
            // adding caption for the image
            self.documentInteractionController.annotation = NSDictionary(object: "hej ifrån picat", forKey: "InstagramCaption")
            //self.documentInteractionController.annotation = ["InstagramCaption": instagramCaption]
            self.documentInteractionController.presentOpenInMenuFromRect(rect, inView: view, animated: true)
        }
        else {
            
            // alert displayed when the instagram application is not available in the device
            UIAlertView(title: kAlertViewTitle, message: kAlertViewMessage, delegate:nil, cancelButtonTitle:"Ok").show()
        }
    }
}


