//
//  PostCell.swift
//  Picat
//
//  Created by Per Sonberg on 2016-01-11.
//  Copyright © 2016 Per Sonberg. All rights reserved.
//

import UIKit

class PostCell: UITableViewCell {
    
    // MARK: - Outlets
    @IBOutlet weak var TopBarLabel: UILabel!
    @IBOutlet weak var likesLabel: UILabel!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var optionButton: UIButton!
    @IBOutlet weak var progress: UIProgressView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        createImageView()
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    
    func getScreenWidth() -> CGFloat {
        let screenRect : CGRect  = UIScreen.mainScreen().bounds;
        return screenRect.size.width;
    }
    
    func createImageView() {
        let size : CGFloat = getScreenWidth()
        let cellImg : UIImageView = UIImageView(frame: CGRectMake(0, self.TopBarLabel.bounds.size.height, size, size))
        cellImg.backgroundColor = UIColor(red: 220/255, green: 220/255, blue: 220/255, alpha: 1)
        cellImg.setValue(10, forKey: "tag")
        self.addSubview(cellImg)
        self.insertSubview(cellImg, atIndex: 0)
    }
    
    func returnHeight() -> CGFloat {
        return getScreenWidth() + self.TopBarLabel.bounds.size.height
    }

}
