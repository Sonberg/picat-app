//
//  CoreDataModel.swift
//  Get Out
//
//  Created by Per Sonberg on 2016-03-09.
//  Copyright © 2016 Per Sonberg. All rights reserved.
//

import UIKit
import CoreData
import Foundation

class CoreDataModel : NSObject {
    
    // Singleton manager
    class var sharedManager : CoreDataModel {
        struct Singleton {
            static let instance = CoreDataModel()
        }
        return Singleton.instance
    }
    
    func save(entity : String, data : AnyObject, AttrArr : [String : String]) {
        let appDel : AppDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
        let context : NSManagedObjectContext = appDel.managedObjectContext
        
        let newData = NSEntityDescription.insertNewObjectForEntityForName(entity, inManagedObjectContext: context) as NSManagedObject
        
        for (key, value) in AttrArr {
            if data.valueForKey(value) !== NSNull() {
                if key == "id" || key == "owner_event" || key == "latitude" || key == "longitude" || key == "owner_company" || key == "distance" || key == "date_start" || key == "date_stop" {
                    newData.setValue(String(data.valueForKey(value)!.stringValue ) , forKey: key)
                } else {
                    newData.setValue(data.valueForKey(value) , forKey: key)
                }
            } else {
                newData.setValue(String(" ") , forKey: key)
            }
        }

        do {
            try context.save()
        } catch let error as NSError {
            print("Error: Couldnt save data \(error)")
        }
        
    }
    
    func load(entity : String, setting : String) -> AnyObject {
        var data = [String : AnyObject]()
        
        let appDel : AppDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
        let context : NSManagedObjectContext = appDel.managedObjectContext
        
        let request = NSFetchRequest(entityName: entity)
        request.returnsObjectsAsFaults = false
        //request.predicate = NSPredicate(format: "image = %@", backgroundImageView.image!)
        do {
            let results : NSArray = try context.executeFetchRequest(request)
            if(results.count > 0) {
                if setting == "all" {
                    return results
                } else {
                    if entity == "EventWatermark" || entity == "TimelimitWatermark" {
                        return results
                    } else {
                        if let obj = results.lastObject {
                            return obj
                        }
                    }
                }
            } else {
                // No data found in core data
            }
            
        } catch let error as NSError {
            print("Error: Error while fetching objects \(error)")
        }
        return NSArray()
    }
    
    func clear(entity : String) {
        let appDel : AppDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
        let context : NSManagedObjectContext = appDel.managedObjectContext
        
        let fetchRequest = NSFetchRequest(entityName: entity)
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        
        do {
            try context.executeRequest(deleteRequest)
        } catch let error as NSError {
            // TODO: handle the error
            print(error)
        }
    }
   }