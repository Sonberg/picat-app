//
//  ImageViewBoxRatio.swift
//  Picat
//
//  Created by Per Sonberg on 2016-01-11.
//  Copyright © 2016 Per Sonberg. All rights reserved.
//

import UIKit

class ImageViewBoxRatio: UIImageView {
    
    var screenWidth : CGFloat?
    var screenHeight : CGFloat?
    
    override init(image: UIImage?) {
        super.init(image: image)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        getScreenSize()
    }
    

    override func awakeFromNib() {
        resizeImageView()
    }
    
    func getScreenSize() -> Void {
        let screenRect : CGRect  = UIScreen.mainScreen().bounds;
        self.screenWidth = screenRect.size.width;
        self.screenHeight = screenRect.size.height;
    }
    
    func resizeImageView() -> Void {
        if self.screenWidth == nil {
            getScreenSize()
            resizeImageView()
        } else {
            self.frame = CGRectMake(0, 0, self.screenWidth!, self.screenWidth!);
            self.center = self.superview!.center;
        }
    }
}
