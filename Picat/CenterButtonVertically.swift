//
//  CenterButtonVertically.swift
//  Picat
//
//  Created by Per Sonberg on 2016-01-13.
//  Copyright © 2016 Per Sonberg. All rights reserved.
//

import UIKit

class CenterButtonVertically: UIButton {

    func centerVertically() {
        self.addSubview(<#T##view: UIView##UIView#>)
    }

}
