//
//  localTimezone.swift
//  Picat
//
//  Created by Per Sonberg on 2016-03-28.
//  Copyright © 2016 Per Sonberg. All rights reserved.
//

import Foundation

extension NSDate {
    func ToLocalStringWithFormat(dateFormat: String) -> String {
        // change to a readable time format and change to local time zone
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = dateFormat
        dateFormatter.timeZone = NSTimeZone.localTimeZone()
        let timeStamp = dateFormatter.stringFromDate(self)
        return timeStamp
    }
}