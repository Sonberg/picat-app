//
//  PicatService.swift
//  Picat
//
//  Created by Per Sonberg on 2016-03-15.
//  Copyright © 2016 Per Sonberg. All rights reserved.
//

import UIKit
import Alamofire
import FBSDKCoreKit

class PicatService: NSObject {
    
    // singleton manager
    class var sharedManager: PicatService {
        struct Singleton {
            static let instance = PicatService()
        }
        return Singleton.instance
    }
    
    var events = [Event]()
    
    
    enum viewType {
        case event
        case timelimit
        case student
        case all
    }
    
    /*
     Login Server-side
     */
    
    func loginServer(data : AnyObject, completion: (access: Bool) -> Void) {
        
        var id : String = ""
        var email : String = ""
        var name : String = ""
        
        if data.valueForKey("id") != nil {
            id = data.valueForKey("id") as! String
        }
        
        if data.valueForKey("email")  != nil {
            email = data.valueForKey("email") as! String
        }
        
        if data.valueForKey("name") != nil {
            name = data.valueForKey("name") as! String
        }
        
        let url = Settings.sharedManager.url + "user/" + id
        let parameters = [
            "facebook_id": id,
            "email": email,
            "name": name
        ]
        
        Alamofire.request(.POST, url, headers: Settings.sharedManager.headers, parameters: parameters).responseJSON { response in
            if let JSON = response.result.value {
                print(JSON)
                completion(access: JSON.valueForKey("access") as! Bool)
                if JSON.valueForKey("access") as! Bool == true {
                    self.fetch(.all)
                }
            }
        }
    }
    
    
    /*
        Fetch watermarks for events nearby / Country / Two latest timelimite
    */
    
    func fetchWatermarks(lat : String, long : String) {
        let url = Settings.sharedManager.url + "watermarks/" + lat + "/" + long
        Alamofire.request(.GET, url, headers: Settings.sharedManager.headers)
            .responseJSON { response in
                if let JSON = response.result.value {
                    if let events = JSON.valueForKey("event"){
                        CoreDataModel.sharedManager.clear("EventWatermark")
                        if events.count > 0 {
                            for e in events as! NSArray {
                                CoreDataModel.sharedManager.save("EventWatermark", data: e, AttrArr: Settings.sharedManager.valueEvent)
                            }
                        }
                    }
                    
                    if let timelimit = JSON.valueForKey("timelimit") {
                        CoreDataModel.sharedManager.clear("TimelimitWatermark")
                        for t in timelimit as! NSArray {
                            CoreDataModel.sharedManager.save("TimelimitWatermark", data: t as! NSDictionary, AttrArr: Settings.sharedManager.valueTimelimit)
                        }
                    }

                }
        }
    }
    
    
    func returnWatermarks() -> [String : AnyObject] {
        let event = CoreDataModel.sharedManager.load("EventWatermark", setting: "")
        let country = CoreDataModel.sharedManager.load("CountryWatermark", setting: "")
        let timelimit = CoreDataModel.sharedManager.load("TimelimitWatermark", setting: "")
        return ["event" : event, "country" : country, "timelimit" : timelimit]
    }
    
    
    /*
    
        Send Image To Server
    
    */
    
    func PostImage(image : UIImage, text : String, type : String, owner : String, progress : UIProgressView? = nil, completion: (succeeded: Bool) -> Void) {
        if progress != nil {
            progress?.progress = 0
        }
        
        let req = FBSDKGraphRequest(graphPath: "me", parameters: ["fields":"email,name,picture"])
        req.startWithCompletionHandler({ (connection, result, error : NSError!) -> Void in
            if(error == nil) {
                let id : String = result.valueForKey("id") as! String
                let fileUploader = FileUploader()
                
                fileUploader.addFileData( UIImageJPEGRepresentation(image,1)!, withName: "image", withMimeType: "jpg" )
                fileUploader.setValue( text, forParameter: "text" )
                fileUploader.setValue( type, forParameter: "type" )
                fileUploader.setValue( owner, forParameter: "owner" )
                fileUploader.setValue( id, forParameter: "owner_user" )
                fileUploader.setValue(Settings.sharedManager.token, forHeader: "X-Token")
                let url = Settings.sharedManager.url + "photos/image/" + type
                // put your server URL here
                let request = NSMutableURLRequest( URL: NSURL(string: url )! )
                request.HTTPMethod = "POST"
                fileUploader.uploadFile(request: request)!
                    .progress { [weak self] bytesWritten, totalBytesWritten, totalBytesExpectedToWrite in
                        //  To update your ui, dispatch to the main queue.
                        dispatch_async(dispatch_get_main_queue()) {
                            if progress != nil {
                                progress?.progress = Float(totalBytesWritten) / Float(totalBytesExpectedToWrite)
                            }
                        }
                    }
                    .responseData { response in
                        print(response)
                         completion(succeeded: true)
                }
            } else {
                completion(succeeded: false)
                print("error \(error)")
            }
        })
    }
    
    
    /*
    
        Fetch all events + 20 most nearby
    
    */
    
    func fetch(type : viewType) {
        
        switch type {
        case .event:
            self.event()
            break
        case .student:
            self.student()
            break
        case .timelimit:
            self.timelimit()
            break
        case .all:
            self.timelimit()
            self.student()
            self.event()
            break
        }
        
    }
    
    
    
    func returnEvents(type : viewType) -> [String : AnyObject] {
        var all : AnyObject?
        
        switch type {
            
        case .event :
            all = CoreDataModel.sharedManager.load("AllEvents", setting: "all")
            break
        case .student :
            all = CoreDataModel.sharedManager.load("AllSchools", setting: "all")
            break
        case .timelimit :
            all = CoreDataModel.sharedManager.load("AllTimelimits", setting: "all")
            break
            
        case .all:
            break
        }
        
        return ["all" : all!]
        
        
    }
    
   
    func timelimit() {
        let pos = LocationManager.sharedManager.returnLocation()
        Alamofire.request(.GET, Settings.sharedManager.url + "timelimits/" + pos.lat + "/" + pos.long, headers: Settings.sharedManager.headers)
            .responseJSON { response in
                if let JSON = response.result.value {
                    if let all = JSON.valueForKey("all") {

                        CoreDataModel.sharedManager.clear("AllTimelimits")
                        
                        if all is NSArray {
                            for e in all as! NSArray {
                                CoreDataModel.sharedManager.save("AllTimelimits", data: e, AttrArr: Settings.sharedManager.TimelimitAttr)
                            }
                        }
                        
                        if all is NSDictionary {
                            for e in all as! NSDictionary {
                                CoreDataModel.sharedManager.save("AllTimelimits", data: e.value, AttrArr: Settings.sharedManager.TimelimitAttr)
                            }
                        }

                    }
                }
        }
    }
    
    
    func student() {
        let pos = LocationManager.sharedManager.returnLocation()
        Alamofire.request(.GET, Settings.sharedManager.url + "schools/" + pos.lat + "/" + pos.long, headers: Settings.sharedManager.headers)
            .responseJSON { response in
                if let JSON = response.result.value {
                    if let all = JSON.valueForKey("all") {
                        CoreDataModel.sharedManager.clear("AllSchools")
                        
                        if all is NSArray {
                            for e in all as! NSArray {
                                CoreDataModel.sharedManager.save("AllSchools", data: e, AttrArr: Settings.sharedManager.SchoolAttr)
                            }
                        }
                        
                        if all is NSDictionary {
                            for e in all as! NSDictionary {
                                CoreDataModel.sharedManager.save("AllSchools", data: e.value, AttrArr: Settings.sharedManager.SchoolAttr)
                            }
                        }
                    }
                    
                }
            }
        }
    
    /*
    
        Fetch all posts from a school
    
    */

    func event() {
        let pos = LocationManager.sharedManager.returnLocation()
        Alamofire.request(.GET, Settings.sharedManager.url + "events/" + pos.lat + "/" + pos.long, headers: Settings.sharedManager.headers)
            .responseJSON { response in
                if let JSON = response.result.value {
                    if let all = JSON.valueForKey("all") {
                        CoreDataModel.sharedManager.clear("AllEvents")
                        
                        if all is NSArray {
                            for e in all as! NSArray {
                                CoreDataModel.sharedManager.save("AllEvents", data: e, AttrArr: Settings.sharedManager.EventsAttr)
                            }
                        }
                        
                        if all is NSDictionary {
                            for e in all as! NSDictionary {
                                CoreDataModel.sharedManager.save("AllEvents", data: e.value, AttrArr: Settings.sharedManager.EventsAttr)
                            }
                        }
                    }
                    
                }
        }
    }
}