//
//  LeftPadding.swift
//  Picat
//
//  Created by Per Sonberg on 2016-01-11.
//  Copyright © 2016 Per Sonberg. All rights reserved.
//

import UIKit

class LeftPadding: UILabel {
    override func drawTextInRect(rect: CGRect) {
        let newRect = CGRectOffset(rect, 10, 0) // move text 10 points to the right
        super.drawTextInRect(newRect)
    }
}
